# Système solaire virtuel

![](solaire.jpg)

## Un projet de : 
- Guillaume Bettin

## Encadré par : 
- Pierre-Frédéric Villard 

## Installer l'application sur l'Oculus Go

```
adb install sytemeSolaire1.apk
```
