# Comment installer les applications sur un téléphone Android

![](https://upload.wikimedia.org/wikipedia/commons/thumb/a/ad/Google-Cardboard.jpg/300px-Google-Cardboard.jpg)

## Matériel
- Un dispositif pour contenir le téléphone de type **Google Cardboard** ([ref](https://fr.wikipedia.org/wiki/Google_Cardboard))
- Un SmartPhone sous Android
- Un fichier avec l'extension ```apk```
- L'utilitaire ```ADB```(Android Debug Bridge) 
  - Linux : ```sudo apt-get install android-tools-adb```
  - Mac OS : ```brew install android-platform-tools```
  - Windows : Outil à télécharger [ici](https://dl.google.com/android/repository/platform-tools-latest-windows.zip)

## Préparer le téléphone pour recevoir le fichier 

1. Accéder aux paramètres de l’appareil Android
2. Aller dans les paramètres système
3. Aller dans « À propos du téléphone »
4. Tapoter 7 fois « numéro de build »
5. Les options pour les développeurs apparaissent dans les paramètres système

## Installation d'une application

1. Connecter le casque
2. Vérifier que le casque est bien visible sur le port USB
```
adb devices
```
3. Utiliser la commande :
```
adb kill-server
```
4. Connecter le casque
5. Utiliser la commande :
```
adb start-server
```
6. Connecter le casque
7. Installer le fichier *nom_du_fichier.apk*
```
adb install nom_du_fichier.apk
```

## Applications disponibles :
- [SystemeSolaire1](SystemeSolaire1) : Système solaire virtuel. Information pour chaque planète (nom, distance au Soleil, température, présence d'atmosphère, présence d'eau ...) et aussi observation d'autres corps célestes (astéroïdes, comètes, planètes naines) 