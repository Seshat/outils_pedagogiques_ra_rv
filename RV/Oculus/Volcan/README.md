# Eruption effusive et éruption explosive d’un volcan

![](volcan.jpg)

## Un projet de :  
- Anaïs GICQUEL (LP IG3D)
- 

## Encadré par : 
- Pierre-Frédéric Villard 
 
## Commandité par : 
- Mikael CHARLET

## Installer l'application sur l'Oculus Go

```
adb install volcans.apk
```
