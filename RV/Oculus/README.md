# Comment installer les applications sur un Oculus Go

![](https://upload.wikimedia.org/wikipedia/commons/thumb/4/42/Oculus_Go_-_4.jpg/300px-Oculus_Go_-_4.jpg)

## Matériel
- Un Oculus Go ([ref](https://en.wikipedia.org/wiki/Oculus_Go))
- Un fichier avec l'extension ```apk```
- L'utilitaire ```ADB```(Android Debug Bridge) 
  - Linux : ```sudo apt-get install android-tools-adb```
  - Mac OS : ```brew install android-platform-tools```
  - Windows : Outil à télécharger [ici](https://dl.google.com/android/repository/platform-tools-latest-windows.zip)

## Préparer l'Oculus pour recevoir le fichier 

1. Assurez-vous que votre téléphone a le bluetooth activé.
2. Ouvrez l'application Oculus sur votre appareil mobile.
3. Dans le menu **Settings**, sélectionnez Oculus Go
4. Sélectionnez  **More Settings**.
5. Activez le mode développeur (**Developer Mode on**). Si l'on vous demande de créer une organisation Oculus, suivez les étapes suivantes pour ce faire :
    1. Sur votre téléphone ou votre PC, connectez-vous à votre compte Oculus et naviguez vers : [https://dashboard.oculus.com/](https://dashboard.oculus.com/)
    2. Créer une nouvelle organisation
    3. Entrez le nom de votre organisation et cliquez sur **submit**.
    4. Lisez et acceptez les conditions
6. Retournez à votre appareil mobile et suivez les instructions d'Oculus pour activer le mode développeur (**Developer Mode on**).


## Installation d'une application

1. Utiliser la commande :
```
adb kill-server
```
2. Utiliser la commande :
```
adb start-server
```
3. Connecter le casque
4. Vérifier que le casque est bien visible sur le port USB
```
adb devices
```
3. Autoriser (dans l'interface de l'Oculus)
7. Installer le fichier *nom_du_fichier.apk*
```
adb install nom_du_fichier.apk
```

## Applications disponibles :
- [Seisme](Seisme) : Observation d'un puissant séisme dans les rues d'une grande ville
- [Volcan](Volcan) : Eruption effusive et éruption explosive d’un volcan
- [Dinosaures](Dinosaures) : Comparer les êtres vivants au Cambrien et au Crétacé 
- [SystemeSolaire](SystemeSolaire) : Système solaire virtuel. Information pour chaque planète (nom, distance au Soleil, température, présence d'atmosphère, présence d'eau ...) et aussi observation d'autres corps célestes (astéroïdes, comètes, planètes naines) 

## Acces aux applications dans le casque Oculus

- Aller dans **Navigation**  &rarr; **Bibliothèque** &rarr; **Sources inconnues**