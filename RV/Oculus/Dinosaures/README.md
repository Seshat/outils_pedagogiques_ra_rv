# Comparer les êtres vivants du Cambrien et du Crétacé

![](dinoAffiche.jpg)

## Un projet de : 
- Benoît Witz (LP IG3D)
- Amélie Soulagnat (LP IG3D)
- Daniel Verduzco Villanueva (LP IG3D)
- Justin Rouet  (LP IG3D)

## Encadré par : 
- Pierre-Frédéric Villard 
- Laurence Moreau

## Commandité par : 
- Mme Marie-Paule Grandpré

## Installer l'application sur l'Oculus Go

```
adb install ApplicationEvoZoo.apk
```
