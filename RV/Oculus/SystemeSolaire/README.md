# Système solaire virtuel

![](soleil.jpg)

## Un projet de : 
- Justin Rouet  (LP IG3D)

## Encadré par : 
- Pierre-Frédéric Villard 
- Laurence Moreau
- Aline Caspary

## Installer l'application sur l'Oculus Go

```
adb install Galileo_v0.9.1.apk
```
