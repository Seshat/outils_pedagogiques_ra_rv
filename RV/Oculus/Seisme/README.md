# Observation d'un puissant séisme dans les rues d'une grande ville

![](seisme.jpg)

## Un projet de : 
- Alexandre ABBES  (LP IG3D)
- Kaélody BEREAU  (LP IG3D)
- Florian GRESSER (LP IG3D)

## Encadré par : 
- Pierre-Frédéric Villard 

## Commandité par : 
- Mikael CHARLET

## Installer l'application sur l'Oculus Go

```
adb install SéismeVR.apk
```
