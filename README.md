# Applications pédagogiques en réalités augmentée et virtuelle

![](https://idmc.univ-lorraine.fr/wp-content/uploads/2020/04/idmc-ul-logo-2020.png)![](https://iutsd.blog.univ-lorraine.fr/files/2019/04/logo_iutsd-siteweb.png)![](https://www.globalpartnership.org/sites/default/files/logo_mainalapate.jpg)

## Contexte


## Etudiants 
- Licence Professionnelle d'infographie 3D de l'IUT de St-Dié (université de Lorraine)
- Le Master 2 en science cognitive de L'IDMC (université de Lorraine)

## Encadrement


## Support
- [Presse](Presse) : Articles de presse 
- [RA](RA) : Applications en réalité augmentée
- [RV](RV) : Applications en réalité virtuelle