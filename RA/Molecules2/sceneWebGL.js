var scene, camera, renderer, clock, deltaTime, totalTime;

var arToolkitSource, arToolkitContext;

var markerRoot1,markerRoot2, markerRoot3, markerRoot4, markerRoot5, markerRoot6, markerRoot7, markerRoot8, markerRoot9, markerRoot10;

var mesh1;

var spheretab = Array(3);
var spheretab2 = Array(3);

var spheretab3 = Array(8);
var spheretab4 = Array(8);

var spheretab5 = Array(10);
var spheretab6 = Array(10);

var spheretab7 = Array(54);
var spheretab8 = Array(54);

var spheretab9 = Array(9);
var spheretab10 = Array(9);




var ok1 = false;
var ok2 = false;
var ok3 = false;
var ok4 = false;
var ok5 = false;


var rayonHydrogene = 0.06;
var rayonOxygene = 0.1;
var rayonCarbonne = 0.2;
var rayonAzote = 0.2;
var rayonFer = 0.2;
var valeurSur = 0.04;
var valeurSurx2 = valeurSur *2;
var valeurSurx3 = valeurSur *3;

var coupedecaleOxyOxy = rayonOxygene + rayonOxygene - valeurSur;
var coupedecaleOxyCarb = rayonOxygene + rayonCarbonne - valeurSur;
var coupedecaleDiHydro = rayonHydrogene + rayonHydrogene - valeurSur;
var coupedecaleAzoHydro = rayonAzote + rayonHydrogene - valeurSur;
var coupedecaleFerOxy = rayonFer + rayonOxygene - valeurSurx2;
var coupedecaleCarbHydro = rayonCarbonne + rayonHydrogene - valeurSur;
var coupedecaleOxyHydro = rayonOxygene + rayonHydrogene - valeurSur;
var coupedecaleCarbCarb = rayonCarbonne + rayonCarbonne - valeurSur;
var coupedecaleAzoAzo = rayonAzote + rayonAzote - valeurSur;

function initialize() {
	scene = new THREE.Scene();

	let ambientLight = new THREE.AmbientLight( 0xcccccc, 1.0 );
	scene.add( ambientLight );
	
	let light = new THREE.PointLight( 0xffffff, 1, 100 );
	light.position.set( 0,4,0 ); // default; light shining from top
	light.castShadow = true;
	scene.add( light );
				
	camera = new THREE.Camera();
	scene.add(camera);

	renderer = new THREE.WebGLRenderer({
		antialias : true,
		alpha: true
	});
	renderer.setClearColor(new THREE.Color('lightgrey'), 0)
	renderer.setSize( 640, 480 );
	renderer.domElement.style.position = 'absolute'
	renderer.domElement.style.top = '0px'
	renderer.domElement.style.left = '0px'
	document.body.appendChild( renderer.domElement );

	clock = new THREE.Clock();
	deltaTime = 0;
	totalTime = 0;
	
	////////////////////////////////////////////////////////////
	// setup arToolkitSource
	////////////////////////////////////////////////////////////

	arToolkitSource = new THREEx.ArToolkitSource({
		sourceType : 'webcam', sourceWidth : 1280, sourceHeight : 960, displayWidth : 1280, displayHeight : 960,
	});

	function onResize()
	{
		arToolkitSource.onResize()	
		arToolkitSource.copySizeTo(renderer.domElement)	
		if ( arToolkitContext.arController !== null )
		{
			arToolkitSource.copySizeTo(arToolkitContext.arController.canvas)	
		}	
	}

	arToolkitSource.init(function onReady(){
		onResize()
	});
	
	// handle resize event
	window.addEventListener('resize', function(){
		onResize()
	});
	
	////////////////////////////////////////////////////////////
	// setup arToolkitContext
	////////////////////////////////////////////////////////////	

	// create atToolkitContext
	arToolkitContext = new THREEx.ArToolkitContext({
		cameraParametersUrl: 'data/camera_para.dat',
		detectionMode: 'mono'
	});
	
	// copy projection matrix to camera when initialization complete
	arToolkitContext.init( function onCompleted(){
		camera.projectionMatrix.copy( arToolkitContext.getProjectionMatrix() );
	});

	////////////////////////////////////////////////////////////
	// setup markerRoots
	////////////////////////////////////////////////////////////

	// build markerControls
	markerRoot1 = new THREE.Group();
	scene.add(markerRoot1);
	
	markerRoot2 = new THREE.Group();
	scene.add(markerRoot2);
	let markerControls1 = new THREEx.ArMarkerControls(arToolkitContext, markerRoot1, {
		type: 'pattern', patternUrl: "data/patterntest2.patt",
	})
	
	let markerControls2 = new THREEx.ArMarkerControls(arToolkitContext, markerRoot2, {
		type: 'pattern', patternUrl: "data/patterntest1.patt",
	})

	let geometry1 = new THREE.PlaneBufferGeometry(1,1, 4,4);
	//let loader = new THREE.TextureLoader();
	// let texture = loader.load( 'images/earth.jpg', render );
	let material1 = new THREE.MeshBasicMaterial( { color: 0x0000ff, opacity: 0.5 } );
	mesh1 = new THREE.Mesh( geometry1, material1 );
	mesh1.rotation.x = -Math.PI/2;
	//markerRoot1.add( mesh1 );
	
	// let floorGeometry = new THREE.PlaneGeometry( 20,20 );
	// let floorMaterial = new THREE.ShadowMaterial();
	// floorMaterial.opacity = 0.3;
	// let floorMesh = new THREE.Mesh( floorGeometry, floorMaterial );
	// floorMesh.rotation.x = -Math.PI/2;
	// floorMesh.receiveShadow = true;
	// markerRoot1.add( floorMesh );
	
	
	
	
	
	
	
	function onProgress(xhr) { console.log( (xhr.loaded / xhr.total * 100) + '% loaded' ); }
	function onError(xhr) { console.log( 'An error happened' ); }
	
	new THREE.MTLLoader()
		.setPath( 'models/' )
		.load( 'fish-2.mtl', function ( materials ) {
			materials.preload();
			new THREE.OBJLoader()
				.setMaterials( materials )
				.setPath( 'models/' )
				.load( 'fish-2.obj', function ( group ) {
					mesh0 = group.children[0];
					mesh0.material.side = THREE.DoubleSide;
					mesh0.position.y = 0.25;
					mesh0.scale.set(0.25,0.25,0.25);
					//markerRoot1.add(mesh0);
				}, onProgress, onError );
		});
		
	
	//shadow
	renderer.shadowMap.enabled = true;
		
		

	
	
	//CO2
	var sphereMaterial1 = new THREE.MeshPhongMaterial( { color: 0x0000FF } );
	var sphereMaterial2 = new THREE.MeshPhongMaterial( { color: 0xFF0000, specular : 0x333333, reflectivity : 0.9 } );
	var sphereMaterial3 = new THREE.MeshPhongMaterial( { color: 0x000000, specular : 0x333333, reflectivity : 0.1 } );
	
	var sphere1 = new THREE.Mesh(new THREE.SphereGeometry( rayonOxygene, 32, 32 ), sphereMaterial2 );
	var sphere2 = new THREE.Mesh(new THREE.SphereGeometry( rayonOxygene, 32, 32 ), sphereMaterial2 );
	var sphere3 = new THREE.Mesh(new THREE.SphereGeometry( rayonCarbonne, 32, 32 ), sphereMaterial3 );
	
	
	

	sphere1.position.x = 0;
	sphere1.position.y = 0;
	sphere1.position.z = 0;
	markerRoot1.add(sphere1);
	

	sphere2.position.x = sphere1.position.x + coupedecaleOxyOxy;
	sphere2.position.y = sphere1.position.y;
	sphere2.position.z = sphere1.position.z;
	markerRoot1.add(sphere2);
	
	sphere3.position.x = 0;
	sphere3.position.y = 1;
	sphere3.position.z = 0;
	markerRoot2.add(sphere3)
	
	sphere1.castShadow = true;
	sphere2.castShadow = true;
	sphere3.castShadow = true;
	
	sphere1.receiveShadow = true;
	sphere2.receiveShadow = true;
	sphere3.receiveShadow = true;
	
	spheretab[0] = sphere1;
	spheretab[1] = sphere2;
	spheretab[2] = sphere3;
	
	//2NH4
	markerRoot3 = new THREE.Group();
	scene.add(markerRoot3);
	
	markerRoot4 = new THREE.Group();
	scene.add(markerRoot4);
	let markerControls3 = new THREEx.ArMarkerControls(arToolkitContext, markerRoot3, {
		type: 'pattern', patternUrl: "data/patterntest3.patt",
	})
	// pattern2C4H10 // pattern3H2 // pattern3O2 // pattern4Fe // pattern13O2 // patternC // patternCH4 // patternN2 // patternO2
	
	let markerControls4 = new THREEx.ArMarkerControls(arToolkitContext, markerRoot4, {
		type: 'pattern', patternUrl: "data/patterntest4.patt",
	})
	
	// let floorMesh2 = new THREE.Mesh( floorGeometry, floorMaterial );
	// floorMesh2.rotation.x = -Math.PI/2;
	// floorMesh2.receiveShadow = true;
	// markerRoot3.add( floorMesh2 );
	
	
	//azote
	var sphereMaterial4 = new THREE.MeshPhongMaterial( { color: 0x318CE7, specular : 0x333333, reflectivity : 0.9 } );
	//hydrogène
	var sphereMaterial5 = new THREE.MeshPhongMaterial( { color: 0xFFFFFF, specular : 0x333333, reflectivity : 0.1 } );
	
	
	var sphere4 = new THREE.Mesh(new THREE.SphereGeometry( rayonAzote, 32, 32 ), sphereMaterial4 );
	var sphere5 = new THREE.Mesh(new THREE.SphereGeometry( rayonAzote, 32, 32 ), sphereMaterial4 );
	
	var sphere6 = new THREE.Mesh(new THREE.SphereGeometry( rayonHydrogene, 32, 32 ), sphereMaterial5 );
	var sphere7 = new THREE.Mesh(new THREE.SphereGeometry( rayonHydrogene, 32, 32 ), sphereMaterial5 );
	var sphere8 = new THREE.Mesh(new THREE.SphereGeometry( rayonHydrogene, 32, 32 ), sphereMaterial5 );
	var sphere9 = new THREE.Mesh(new THREE.SphereGeometry( rayonHydrogene, 32, 32 ), sphereMaterial5 );
	var sphere10 = new THREE.Mesh(new THREE.SphereGeometry( rayonHydrogene, 32, 32 ), sphereMaterial5 );
	var sphere11 = new THREE.Mesh(new THREE.SphereGeometry( rayonHydrogene, 32, 32 ), sphereMaterial5 );
	
	//azote
	sphere4.position.x = 0;
	sphere4.position.y = 0;
	sphere4.position.z = 0;
	markerRoot3.add(sphere4);
	
	sphere5.position.x = sphere4.position.x + coupedecaleAzoAzo;
	sphere5.position.y = sphere4.position.y + valeurSur;
	sphere5.position.z = sphere4.position.z;
	markerRoot3.add(sphere5);
	
	//hydrogène
	sphere6.position.x = 0;
	sphere6.position.y = 0;
	sphere6.position.z = 0;
	markerRoot4.add(sphere6);
	
	sphere7.position.x = sphere6.position.x + coupedecaleDiHydro;
	sphere7.position.y = sphere6.position.y + coupedecaleDiHydro;
	sphere7.position.z = sphere6.position.z;
	markerRoot4.add(sphere7)
	
	sphere8.position.x = -0.5;
	sphere8.position.y = 0.5;
	sphere8.position.z = 0;
	markerRoot4.add(sphere8);
	
	sphere9.position.x = sphere8.position.x + coupedecaleDiHydro;
	sphere9.position.y = sphere8.position.y;
	sphere9.position.z = sphere8.position.z;
	markerRoot4.add(sphere9);
	
	sphere10.position.x = 0;
	sphere10.position.y = 0.8;
	sphere10.position.z = -0.5;
	markerRoot4.add(sphere10);
	
	sphere11.position.x = sphere10.position.x ;
	sphere11.position.y = sphere10.position.y - coupedecaleDiHydro;
	sphere11.position.z = sphere10.position.z + coupedecaleDiHydro;
	markerRoot4.add(sphere11);
	
	sphere4.castShadow = true;
	sphere5.castShadow = true;
	sphere6.castShadow = true;
	sphere7.castShadow = true;
	sphere8.castShadow = true;
	sphere9.castShadow = true;
	sphere10.castShadow = true;
	sphere11.castShadow = true;

	
	sphere4.receiveShadow = true;
	sphere5.receiveShadow = true;
	sphere6.receiveShadow = true;
	sphere7.receiveShadow = true;
	sphere8.receiveShadow = true;
	sphere9.receiveShadow = true;
	sphere10.receiveShadow = true;
	sphere11.receiveShadow = true;

	spheretab3[0] = sphere4;
	spheretab3[1] = sphere5;
	spheretab3[2] = sphere6;
	spheretab3[3] = sphere7;
	spheretab3[4] = sphere8;
	spheretab3[5] = sphere9;
	spheretab3[6] = sphere10;
	spheretab3[7] = sphere11;
	
	
	
	//oxyde ferrique 4Fe + 3O2
	markerRoot5 = new THREE.Group();
	scene.add(markerRoot5);
	
	markerRoot6 = new THREE.Group();
	scene.add(markerRoot6);
	let markerControls5 = new THREEx.ArMarkerControls(arToolkitContext, markerRoot5, {
		type: 'pattern', patternUrl: "data/patterntest5.patt",
	})
	// pattern2C4H10 // pattern3H2 // pattern3O2 // pattern4Fe // pattern13O2 // patternC // patternCH4 // patternN2 // patternO2
	
	let markerControls6 = new THREEx.ArMarkerControls(arToolkitContext, markerRoot6, {
		type: 'pattern', patternUrl: "data/patterntest6.patt",
	})
	
	// let floorMesh3 = new THREE.Mesh( floorGeometry, floorMaterial );
	// floorMesh3.rotation.x = -Math.PI/2;
	// floorMesh3.receiveShadow = true;
	// markerRoot5.add( floorMesh3 );
	
	
	//fer
	var sphereMaterial6 = new THREE.MeshPhongMaterial( { color: 0x7F7F7F, specular : 0x333333, reflectivity : 0.9 } );
	//oxygene
	var sphereMaterial7 = new THREE.MeshPhongMaterial( { color: 0xFF0000, specular : 0x333333, reflectivity : 0.1 } );
	
	
	var sphere12 = new THREE.Mesh(new THREE.SphereGeometry( rayonFer, 32, 32 ), sphereMaterial6 );
	var sphere13 = new THREE.Mesh(new THREE.SphereGeometry( rayonFer, 32, 32 ), sphereMaterial6 );
	var sphere14 = new THREE.Mesh(new THREE.SphereGeometry( rayonFer, 32, 32 ), sphereMaterial6 );
	var sphere15 = new THREE.Mesh(new THREE.SphereGeometry( rayonFer, 32, 32 ), sphereMaterial6 );
	
	var sphere16 = new THREE.Mesh(new THREE.SphereGeometry( rayonOxygene, 32, 32 ), sphereMaterial7 );
	var sphere17 = new THREE.Mesh(new THREE.SphereGeometry( rayonOxygene, 32, 32 ), sphereMaterial7 );
	var sphere18 = new THREE.Mesh(new THREE.SphereGeometry( rayonOxygene, 32, 32 ), sphereMaterial7 );
	var sphere19 = new THREE.Mesh(new THREE.SphereGeometry( rayonOxygene, 32, 32 ), sphereMaterial7 );
	var sphere20 = new THREE.Mesh(new THREE.SphereGeometry( rayonOxygene, 32, 32 ), sphereMaterial7 );
	var sphere21 = new THREE.Mesh(new THREE.SphereGeometry( rayonOxygene, 32, 32 ), sphereMaterial7 );
	
	//fer
	sphere12.position.x = 0;
	sphere12.position.y = 0;
	sphere12.position.z = 0;
	markerRoot5.add(sphere12);
	
	sphere13.position.x = 0.7;
	sphere13.position.y = 0.7;
	sphere13.position.z = 0;
	markerRoot5.add(sphere13);
	
	sphere14.position.x = -0.3;
	sphere14.position.y = 0.5;
	sphere14.position.z = - 0.7;
	markerRoot5.add(sphere14);
	
	sphere15.position.x = 0.3;
	sphere15.position.y = 1;
	sphere15.position.z = -0.5;
	markerRoot5.add(sphere15);
	
	//oxygene
	sphere16.position.x = 0;
	sphere16.position.y = 0;
	sphere16.position.z = 0;
	markerRoot6.add(sphere16);
	
	sphere17.position.x = sphere16.position.x + rayonOxygene;
	sphere17.position.y = sphere17.position.y + rayonOxygene;
	sphere17.position.z = sphere17.position.z;
	markerRoot6.add(sphere17);
	
	sphere18.position.x = 0.3;
	sphere18.position.y = 0.3;
	sphere18.position.z = 0;
	markerRoot6.add(sphere18);
	
	sphere19.position.x = sphere18.position.x ;
	sphere19.position.y = sphere18.position.y - rayonOxygene;
	sphere19.position.z = sphere18.position.z - rayonOxygene;
	markerRoot6.add(sphere19);
	
	sphere20.position.x = -0.3;
	sphere20.position.y = 0.05;
	sphere20.position.z = -0.3;
	markerRoot6.add(sphere20);
	
	sphere21.position.x = sphere20.position.x + rayonOxygene;
	sphere21.position.y = sphere20.position.y + rayonOxygene;
	sphere21.position.z = sphere20.position.z - rayonOxygene;
	markerRoot6.add(sphere21);
	
	sphere12.castShadow = true;
	sphere13.castShadow = true;
	sphere14.castShadow = true;
	sphere15.castShadow = true;
	sphere16.castShadow = true;
	sphere17.castShadow = true;
	sphere18.castShadow = true;
	sphere19.castShadow = true;
	sphere20.castShadow = true;
	sphere21.castShadow = true;

	
	sphere12.receiveShadow = true;
	sphere13.receiveShadow = true;
	sphere14.receiveShadow = true;
	sphere15.receiveShadow = true;
	sphere16.receiveShadow = true;
	sphere17.receiveShadow = true;
	sphere18.receiveShadow = true;
	sphere19.receiveShadow = true;
	sphere20.receiveShadow = true;
	sphere21.receiveShadow = true;

	spheretab5[0] = sphere12;
	spheretab5[1] = sphere13;
	spheretab5[2] = sphere14;
	spheretab5[3] = sphere15;
	spheretab5[4] = sphere16;
	spheretab5[5] = sphere17;
	spheretab5[6] = sphere18;
	spheretab5[7] = sphere19;
	spheretab5[8] = sphere20;
	spheretab5[9] = sphere21;
	
	
	
	
//butane + dioxygene = co2 + h2o
	markerRoot7 = new THREE.Group();
	scene.add(markerRoot7);
	
	markerRoot8 = new THREE.Group();
	scene.add(markerRoot8);
	let markerControls7 = new THREEx.ArMarkerControls(arToolkitContext, markerRoot7, {
		type: 'pattern', patternUrl: "data/patterntestou.patt",
	})
	// pattern2C4H10 // pattern3H2 // pattern3O2 // pattern4Fe // pattern13O2 // patternC // patternCH4 // patternN2 // patternO2
	
	let markerControls8 = new THREEx.ArMarkerControls(arToolkitContext, markerRoot8, {
		type: 'pattern', patternUrl: "data/patterntestou2.patt",
	})
	
	// let floorMesh4 = new THREE.Mesh( floorGeometry, floorMaterial );
	// floorMesh4.rotation.x = -Math.PI/2;
	// floorMesh4.receiveShadow = true;
	// markerRoot7.add( floorMesh4 );
	
	
	//carbonne
	var sphereMaterial8 = new THREE.MeshPhongMaterial( { color: 0x000000, specular : 0x333333, reflectivity : 0.9 } );
	//oxygene
	var sphereMaterial9 = new THREE.MeshPhongMaterial( { color: 0xFF0000, specular : 0x333333, reflectivity : 0.1 } );
	//hydrogene
	var sphereMaterial10 = new THREE.MeshPhongMaterial( { color: 0xFFFFFF, specular : 0x333333, reflectivity : 0.1 } );
	
	//commence a 21
	
	
	
	//H / Hydrogene
	
	
	var sphere22 = new THREE.Mesh(new THREE.SphereGeometry( rayonHydrogene, 32, 32 ), sphereMaterial10 );
	var sphere23 = new THREE.Mesh(new THREE.SphereGeometry( rayonHydrogene, 32, 32 ), sphereMaterial10 );
	var sphere24 = new THREE.Mesh(new THREE.SphereGeometry( rayonHydrogene, 32, 32 ), sphereMaterial10 );
	var sphere25 = new THREE.Mesh(new THREE.SphereGeometry( rayonHydrogene, 32, 32 ), sphereMaterial10 );
	var sphere26 = new THREE.Mesh(new THREE.SphereGeometry( rayonHydrogene, 32, 32 ), sphereMaterial10 );
	var sphere27 = new THREE.Mesh(new THREE.SphereGeometry( rayonHydrogene, 32, 32 ), sphereMaterial10 );
	var sphere28 = new THREE.Mesh(new THREE.SphereGeometry( rayonHydrogene, 32, 32 ), sphereMaterial10 );
	var sphere29 = new THREE.Mesh(new THREE.SphereGeometry( rayonHydrogene, 32, 32 ), sphereMaterial10 );
	var sphere30 = new THREE.Mesh(new THREE.SphereGeometry( rayonHydrogene, 32, 32 ), sphereMaterial10 );
	var sphere31 = new THREE.Mesh(new THREE.SphereGeometry( rayonHydrogene, 32, 32 ), sphereMaterial10 );
	var sphere32 = new THREE.Mesh(new THREE.SphereGeometry( rayonHydrogene, 32, 32 ), sphereMaterial10 );
	var sphere33 = new THREE.Mesh(new THREE.SphereGeometry( rayonHydrogene, 32, 32 ), sphereMaterial10 );
	var sphere34 = new THREE.Mesh(new THREE.SphereGeometry( rayonHydrogene, 32, 32 ), sphereMaterial10 );
	var sphere35 = new THREE.Mesh(new THREE.SphereGeometry( rayonHydrogene, 32, 32 ), sphereMaterial10 );
	var sphere36 = new THREE.Mesh(new THREE.SphereGeometry( rayonHydrogene, 32, 32 ), sphereMaterial10 );
	var sphere37 = new THREE.Mesh(new THREE.SphereGeometry( rayonHydrogene, 32, 32 ), sphereMaterial10 );
	var sphere38 = new THREE.Mesh(new THREE.SphereGeometry( rayonHydrogene, 32, 32 ), sphereMaterial10 );
	var sphere39 = new THREE.Mesh(new THREE.SphereGeometry( rayonHydrogene, 32, 32 ), sphereMaterial10 );
	var sphere40 = new THREE.Mesh(new THREE.SphereGeometry( rayonHydrogene, 32, 32 ), sphereMaterial10 );
	var sphere41 = new THREE.Mesh(new THREE.SphereGeometry( rayonHydrogene, 32, 32 ), sphereMaterial10 );
	// O / Oxygene
	
	var sphere42 = new THREE.Mesh(new THREE.SphereGeometry( rayonOxygene, 32, 32 ), sphereMaterial9 );
	var sphere43 = new THREE.Mesh(new THREE.SphereGeometry( rayonOxygene, 32, 32 ), sphereMaterial9 );
	var sphere44 = new THREE.Mesh(new THREE.SphereGeometry( rayonOxygene, 32, 32 ), sphereMaterial9 );
	var sphere45 = new THREE.Mesh(new THREE.SphereGeometry( rayonOxygene, 32, 32 ), sphereMaterial9 );
	var sphere46 = new THREE.Mesh(new THREE.SphereGeometry( rayonOxygene, 32, 32 ), sphereMaterial9 );
	var sphere47 = new THREE.Mesh(new THREE.SphereGeometry( rayonOxygene, 32, 32 ), sphereMaterial9 );
	var sphere48 = new THREE.Mesh(new THREE.SphereGeometry( rayonOxygene, 32, 32 ), sphereMaterial9 );
	var sphere49 = new THREE.Mesh(new THREE.SphereGeometry( rayonOxygene, 32, 32 ), sphereMaterial9 );
	var sphere50 = new THREE.Mesh(new THREE.SphereGeometry( rayonOxygene, 32, 32 ), sphereMaterial9 );
	var sphere51 = new THREE.Mesh(new THREE.SphereGeometry( rayonOxygene, 32, 32 ), sphereMaterial9 );
	var sphere52 = new THREE.Mesh(new THREE.SphereGeometry( rayonOxygene, 32, 32 ), sphereMaterial9 );
	var sphere53 = new THREE.Mesh(new THREE.SphereGeometry( rayonOxygene, 32, 32 ), sphereMaterial9 );
	var sphere54 = new THREE.Mesh(new THREE.SphereGeometry( rayonOxygene, 32, 32 ), sphereMaterial9 );
	var sphere55 = new THREE.Mesh(new THREE.SphereGeometry( rayonOxygene, 32, 32 ), sphereMaterial9 );
	var sphere56 = new THREE.Mesh(new THREE.SphereGeometry( rayonOxygene, 32, 32 ), sphereMaterial9 );
	var sphere57 = new THREE.Mesh(new THREE.SphereGeometry( rayonOxygene, 32, 32 ), sphereMaterial9 );
	var sphere58 = new THREE.Mesh(new THREE.SphereGeometry( rayonOxygene, 32, 32 ), sphereMaterial9 );
	var sphere59 = new THREE.Mesh(new THREE.SphereGeometry( rayonOxygene, 32, 32 ), sphereMaterial9 );
	var sphere60 = new THREE.Mesh(new THREE.SphereGeometry( rayonOxygene, 32, 32 ), sphereMaterial9 );
	var sphere61 = new THREE.Mesh(new THREE.SphereGeometry( rayonOxygene, 32, 32 ), sphereMaterial9 );
	var sphere62 = new THREE.Mesh(new THREE.SphereGeometry( rayonOxygene, 32, 32 ), sphereMaterial9 );
	var sphere63 = new THREE.Mesh(new THREE.SphereGeometry( rayonOxygene, 32, 32 ), sphereMaterial9 );
	var sphere64 = new THREE.Mesh(new THREE.SphereGeometry( rayonOxygene, 32, 32 ), sphereMaterial9 );
	var sphere65 = new THREE.Mesh(new THREE.SphereGeometry( rayonOxygene, 32, 32 ), sphereMaterial9 );
	var sphere66 = new THREE.Mesh(new THREE.SphereGeometry( rayonOxygene, 32, 32 ), sphereMaterial9 );
	var sphere67 = new THREE.Mesh(new THREE.SphereGeometry( rayonOxygene, 32, 32 ), sphereMaterial9 );
	
	//C / Carbone
	var sphere68 = new THREE.Mesh(new THREE.SphereGeometry( rayonCarbonne, 32, 32 ), sphereMaterial8 );
	var sphere69 = new THREE.Mesh(new THREE.SphereGeometry( rayonCarbonne, 32, 32 ), sphereMaterial8 );
	var sphere70 = new THREE.Mesh(new THREE.SphereGeometry( rayonCarbonne, 32, 32 ), sphereMaterial8 );
	var sphere71 = new THREE.Mesh(new THREE.SphereGeometry( rayonCarbonne, 32, 32 ), sphereMaterial8 );
	var sphere72 = new THREE.Mesh(new THREE.SphereGeometry( rayonCarbonne, 32, 32 ), sphereMaterial8 );
	var sphere73 = new THREE.Mesh(new THREE.SphereGeometry( rayonCarbonne, 32, 32 ), sphereMaterial8 );
	var sphere74 = new THREE.Mesh(new THREE.SphereGeometry( rayonCarbonne, 32, 32 ), sphereMaterial8 );
	var sphere75 = new THREE.Mesh(new THREE.SphereGeometry( rayonCarbonne, 32, 32 ), sphereMaterial8 );
	
	
	// C4H10 1
	
	//C 1
	sphere68.position.x = 0;
	sphere68.position.y = 0;
	sphere68.position.z = 0;
	markerRoot7.add(sphere68);
	//C 1
	sphere69.position.x = sphere68.position.x + coupedecaleCarbCarb;
	sphere69.position.y = sphere68.position.y - valeurSur;
	sphere69.position.z = sphere68.position.z;
	markerRoot7.add(sphere69);
	//C 1
	sphere70.position.x = sphere69.position.x + valeurSur;
	sphere70.position.y = sphere69.position.y + coupedecaleCarbCarb;
	sphere70.position.z = sphere69.position.z;
	markerRoot7.add(sphere70);
	//C 1
	sphere71.position.x = sphere70.position.x + coupedecaleCarbCarb;
	sphere71.position.y = sphere70.position.y;
	sphere71.position.z = sphere70.position.z;
	markerRoot7.add(sphere71);
	
	
	//H 1
	sphere22.position.x = sphere68.position.x;
	sphere22.position.y = sphere68.position.y - coupedecaleCarbHydro;
	sphere22.position.z = sphere68.position.z + valeurSurx3;
	markerRoot7.add(sphere22);
	//H 1
	sphere23.position.x = sphere68.position.x;
	sphere23.position.y = sphere68.position.y + valeurSurx3;
	sphere23.position.z = sphere68.position.z + coupedecaleCarbHydro;
	markerRoot7.add(sphere23);
	//H 1
	sphere24.position.x = sphere68.position.x;
	sphere24.position.y = sphere68.position.y + valeurSurx3;
	sphere24.position.z = sphere68.position.z - coupedecaleCarbHydro;
	markerRoot7.add(sphere24);
	//H 1
	sphere25.position.x = sphere69.position.x;
	sphere25.position.y = sphere69.position.y - valeurSurx3;
	sphere25.position.z = sphere69.position.z + coupedecaleCarbHydro;
	markerRoot7.add(sphere25);
	//H 1
	sphere26.position.x = sphere69.position.x;
	sphere26.position.y = sphere69.position.y - valeurSurx3;
	sphere26.position.z = sphere69.position.z - coupedecaleCarbHydro;
	markerRoot7.add(sphere26);
	//H 1
	sphere27.position.x = sphere70.position.x;
	sphere27.position.y = sphere70.position.y + valeurSurx3;
	sphere27.position.z = sphere70.position.z + coupedecaleCarbHydro;
	markerRoot7.add(sphere27);
	//H 1
	sphere28.position.x = sphere70.position.x;
	sphere28.position.y = sphere70.position.y + valeurSurx3;
	sphere28.position.z = sphere70.position.z - coupedecaleCarbHydro;
	markerRoot7.add(sphere28);
	//H 1
	sphere29.position.x = sphere71.position.x + valeurSurx3;
	sphere29.position.y = sphere71.position.y + coupedecaleCarbHydro;
	sphere29.position.z = sphere71.position.z;
	markerRoot7.add(sphere29);
	//H 1
	sphere30.position.x = sphere71.position.x;
	sphere30.position.y = sphere71.position.y - valeurSurx3;
	sphere30.position.z = sphere71.position.z + coupedecaleCarbHydro;
	markerRoot7.add(sphere30);
	//H 1
	sphere31.position.x = sphere71.position.x;
	sphere31.position.y = sphere71.position.y - valeurSurx3;
	sphere31.position.z = sphere71.position.z - coupedecaleCarbHydro;
	markerRoot7.add(sphere31);
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	//C 2
	sphere72.position.x = -0.5;
	sphere72.position.y = 0.5;
	sphere72.position.z = -0.8;
	markerRoot7.add(sphere72);
	//C 2
	sphere73.position.x = sphere72.position.x + coupedecaleCarbCarb;
	sphere73.position.y = sphere72.position.y - valeurSur;
	sphere73.position.z = sphere72.position.z;
	markerRoot7.add(sphere73);
	//C 2
	sphere74.position.x = sphere73.position.x + valeurSur;
	sphere74.position.y = sphere73.position.y + coupedecaleCarbCarb;
	sphere74.position.z = sphere73.position.z;
	markerRoot7.add(sphere74);
	//C 2
	sphere75.position.x = sphere74.position.x + coupedecaleCarbCarb;
	sphere75.position.y = sphere74.position.y;
	sphere75.position.z = sphere74.position.z;
	markerRoot7.add(sphere75);
	
	
	//H 2
	sphere32.position.x = sphere72.position.x;
	sphere32.position.y = sphere72.position.y - coupedecaleCarbHydro;
	sphere32.position.z = sphere72.position.z + valeurSurx3;
	markerRoot7.add(sphere32);
	//H 2
	sphere33.position.x = sphere72.position.x;
	sphere33.position.y = sphere72.position.y + valeurSurx3;
	sphere33.position.z = sphere72.position.z + coupedecaleCarbHydro;
	markerRoot7.add(sphere33);
	//H 2
	sphere34.position.x = sphere72.position.x;
	sphere34.position.y = sphere72.position.y + valeurSurx3;
	sphere34.position.z = sphere72.position.z - coupedecaleCarbHydro;
	markerRoot7.add(sphere34);
	//H 2
	sphere35.position.x = sphere73.position.x;
	sphere35.position.y = sphere73.position.y - valeurSurx3;
	sphere35.position.z = sphere73.position.z + coupedecaleCarbHydro;
	markerRoot7.add(sphere35);
	//H 2
	sphere36.position.x = sphere73.position.x;
	sphere36.position.y = sphere73.position.y - valeurSurx3;
	sphere36.position.z = sphere73.position.z - coupedecaleCarbHydro;
	markerRoot7.add(sphere36);
	//H 2
	sphere37.position.x = sphere74.position.x;
	sphere37.position.y = sphere74.position.y + valeurSurx3;
	sphere37.position.z = sphere74.position.z + coupedecaleCarbHydro;
	markerRoot7.add(sphere37);
	//H 2
	sphere38.position.x = sphere74.position.x;
	sphere38.position.y = sphere74.position.y + valeurSurx3;
	sphere38.position.z = sphere74.position.z - coupedecaleCarbHydro;
	markerRoot7.add(sphere38);
	//H 2
	sphere39.position.x = sphere75.position.x + valeurSurx3;
	sphere39.position.y = sphere75.position.y + coupedecaleCarbHydro;
	sphere39.position.z = sphere75.position.z;
	markerRoot7.add(sphere39);
	//H 2
	sphere40.position.x = sphere75.position.x;
	sphere40.position.y = sphere75.position.y - valeurSurx3;
	sphere40.position.z = sphere75.position.z + coupedecaleCarbHydro;
	markerRoot7.add(sphere40);
	//H 2
	sphere41.position.x = sphere75.position.x;
	sphere41.position.y = sphere75.position.y - valeurSurx3;
	sphere41.position.z = sphere75.position.z - coupedecaleCarbHydro;
	markerRoot7.add(sphere41);
	
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	//O2 1
	sphere42.position.x = -1;
	sphere42.position.y = 0;
	sphere42.position.z = 0;
	markerRoot8.add(sphere42);
	
	sphere43.position.x = sphere42.position.x + coupedecaleOxyOxy;
	sphere43.position.y = sphere42.position.y;
	sphere43.position.z = sphere42.position.z;
	markerRoot8.add(sphere43);
	
	
	//O2 2
	sphere44.position.x = -0.7;
	sphere44.position.y = 0.5;
	sphere44.position.z = 0;
	markerRoot8.add(sphere44);
	
	sphere45.position.x = sphere44.position.x;
	sphere45.position.y = sphere44.position.y + coupedecaleOxyOxy;
	sphere45.position.z = sphere44.position.z + valeurSur;
	markerRoot8.add(sphere45);
	
	
	//O2 3
	
	sphere46.position.x = -0.8;
	sphere46.position.y = 0.3;
	sphere46.position.z = -0.5;
	markerRoot8.add(sphere46);
	
	sphere47.position.x = sphere46.position.x + valeurSur;
	sphere47.position.y = sphere46.position.y + valeurSur;
	sphere47.position.z = sphere46.position.z + valeurSur;
	markerRoot8.add(sphere47);
	
	//O2 4
	
	sphere48.position.x = 0;
	sphere48.position.y = 0.7;
	sphere48.position.z = 0.2;
	markerRoot8.add(sphere48);
	
	sphere49.position.x = sphere48.position.x + coupedecaleOxyOxy;
	sphere49.position.y = sphere48.position.y;
	sphere49.position.z = sphere48.position.z + valeurSur;
	markerRoot8.add(sphere49);
	
	//O2 5
	
	sphere50.position.x = 0;
	sphere50.position.y = 0;
	sphere50.position.z = 1;
	markerRoot8.add(sphere50);
	
	sphere51.position.x = sphere50.position.x + coupedecaleOxyOxy;
	sphere51.position.y = sphere50.position.y;
	sphere51.position.z = sphere50.position.z;
	markerRoot8.add(sphere51);
	
	//O2 6
	
	sphere52.position.x = 0;
	sphere52.position.y = 0.3;
	sphere52.position.z = -1;
	markerRoot8.add(sphere52);
	
	sphere53.position.x = sphere52.position.x + coupedecaleOxyOxy;
	sphere53.position.y = sphere52.position.y;
	sphere53.position.z = sphere52.position.z;
	markerRoot8.add(sphere53);
	
	//O2 7
	
	sphere54.position.x = 1;
	sphere54.position.y = 0;
	sphere54.position.z = 0;
	markerRoot8.add(sphere54);
	
	sphere55.position.x = sphere54.position.x + coupedecaleOxyOxy;
	sphere55.position.y = sphere54.position.y;
	sphere55.position.z = sphere54.position.z;
	markerRoot8.add(sphere55);
	
	//O2 8
	
	sphere56.position.x = 0.5;
	sphere56.position.y = 0.5;
	sphere56.position.z = 0.2;
	markerRoot8.add(sphere56);
	
	sphere57.position.x = sphere56.position.x + valeurSur;
	sphere57.position.y = sphere56.position.y + valeurSur;
	sphere57.position.z = sphere56.position.z + valeurSur;
	markerRoot8.add(sphere57);
	
	//O2 9
	
	sphere58.position.x = 0.5;
	sphere58.position.y = 0.5;
	sphere58.position.z = 0.5;
	markerRoot8.add(sphere58);
	
	sphere59.position.x = sphere58.position.x - coupedecaleOxyOxy;
	sphere59.position.y = sphere58.position.y - valeurSur;
	sphere59.position.z = sphere58.position.z;
	markerRoot8.add(sphere59);
	
	//O2 10
	
	sphere60.position.x = -0.5;
	sphere60.position.y = 0.2;
	sphere60.position.z = -0.5;
	markerRoot8.add(sphere60);
	
	sphere61.position.x = sphere60.position.x;
	sphere61.position.y = sphere60.position.y + coupedecaleOxyOxy;
	sphere61.position.z = sphere60.position.z;
	markerRoot8.add(sphere61);
	
	//O2 11
	
	sphere62.position.x = 0;
	sphere62.position.y = 0.8;
	sphere62.position.z = -0.5;
	markerRoot8.add(sphere62);
	
	sphere63.position.x = sphere62.position.x - coupedecaleOxyOxy;
	sphere63.position.y = sphere62.position.y;
	sphere63.position.z = sphere62.position.z;
	markerRoot8.add(sphere63);
	
	//O2 12
	
	sphere64.position.x = -0.7;
	sphere64.position.y = 0;
	sphere64.position.z = 0.7;
	markerRoot8.add(sphere64);
	
	sphere65.position.x = sphere64.position.x;
	sphere65.position.y = sphere64.position.y;
	sphere65.position.z = sphere64.position.z - coupedecaleOxyOxy;
	markerRoot8.add(sphere65);
	
	//O2 13
	
	sphere66.position.x = -0.4;
	sphere66.position.y = 0;
	sphere66.position.z = 0.4;
	markerRoot8.add(sphere66);
	
	sphere67.position.x = sphere66.position.x + coupedecaleOxyOxy;
	sphere67.position.y = sphere66.position.y;
	sphere67.position.z = sphere66.position.z + valeurSur;
	markerRoot8.add(sphere67);
	
	
	
	//22 - 75
	sphere22.castShadow = true;
	sphere23.castShadow = true;
	sphere24.castShadow = true;
	sphere25.castShadow = true;
	sphere26.castShadow = true;
	sphere27.castShadow = true;
	sphere28.castShadow = true;
	sphere29.castShadow = true;
	sphere30.castShadow = true;
	sphere31.castShadow = true;
	sphere32.castShadow = true;
	sphere33.castShadow = true;
	sphere34.castShadow = true;
	sphere35.castShadow = true;
	sphere36.castShadow = true;
	sphere37.castShadow = true;
	sphere38.castShadow = true;
	sphere39.castShadow = true;
	sphere40.castShadow = true;
	sphere41.castShadow = true;
	sphere42.castShadow = true;
	sphere43.castShadow = true;
	sphere44.castShadow = true;
	sphere45.castShadow = true;
	sphere46.castShadow = true;
	sphere47.castShadow = true;
	sphere48.castShadow = true;
	sphere49.castShadow = true;
	sphere50.castShadow = true;
	sphere51.castShadow = true;
	sphere52.castShadow = true;
	sphere53.castShadow = true;
	sphere54.castShadow = true;
	sphere55.castShadow = true;
	sphere56.castShadow = true;
	sphere57.castShadow = true;
	sphere58.castShadow = true;
	sphere59.castShadow = true;
	sphere60.castShadow = true;
	sphere61.castShadow = true;
	sphere62.castShadow = true;
	sphere63.castShadow = true;
	sphere64.castShadow = true;
	sphere65.castShadow = true;
	sphere66.castShadow = true;
	sphere67.castShadow = true;
	sphere68.castShadow = true;
	sphere69.castShadow = true;
	sphere70.castShadow = true;
	sphere71.castShadow = true;
	sphere72.castShadow = true;
	sphere73.castShadow = true;
	sphere74.castShadow = true;
	sphere75.castShadow = true;
	
	sphere22.receiveShadow = true;
	sphere23.receiveShadow = true;
	sphere24.receiveShadow = true;
	sphere25.receiveShadow = true;
	sphere26.receiveShadow = true;
	sphere27.receiveShadow = true;
	sphere28.receiveShadow = true;
	sphere29.receiveShadow = true;
	sphere30.receiveShadow = true;
	sphere31.receiveShadow = true;
	sphere32.receiveShadow = true;
	sphere33.receiveShadow = true;
	sphere34.receiveShadow = true;
	sphere35.receiveShadow = true;
	sphere36.receiveShadow = true;
	sphere37.receiveShadow = true;
	sphere38.receiveShadow = true;
	sphere39.receiveShadow = true;
	sphere40.receiveShadow = true;
	sphere41.receiveShadow = true;
	sphere42.receiveShadow = true;
	sphere43.receiveShadow = true;
	sphere44.receiveShadow = true;
	sphere45.receiveShadow = true;
	sphere46.receiveShadow = true;
	sphere47.receiveShadow = true;
	sphere48.receiveShadow = true;
	sphere49.receiveShadow = true;
	sphere50.receiveShadow = true;
	sphere51.receiveShadow = true;
	sphere52.receiveShadow = true;
	sphere53.receiveShadow = true;
	sphere54.receiveShadow = true;
	sphere55.receiveShadow = true;
	sphere56.receiveShadow = true;
	sphere57.receiveShadow = true;
	sphere58.receiveShadow = true;
	sphere59.receiveShadow = true;
	sphere60.receiveShadow = true;
	sphere61.receiveShadow = true;
	sphere62.receiveShadow = true;
	sphere63.receiveShadow = true;
	sphere64.receiveShadow = true;
	sphere65.receiveShadow = true;
	sphere66.receiveShadow = true;
	sphere67.receiveShadow = true;
	sphere68.receiveShadow = true;
	sphere69.receiveShadow = true;
	sphere70.receiveShadow = true;
	sphere71.receiveShadow = true;
	sphere72.receiveShadow = true;
	sphere73.receiveShadow = true;
	sphere74.receiveShadow = true;
	sphere75.receiveShadow = true;

	
	

	spheretab7[0] = sphere22;
	spheretab7[1] = sphere23;
	spheretab7[2] = sphere24;
	spheretab7[3] = sphere25;
	spheretab7[4] = sphere26;
	spheretab7[5] = sphere27;
	spheretab7[6] = sphere28;
	spheretab7[7] = sphere29;
	spheretab7[8] = sphere30;
	spheretab7[9] = sphere31;
	spheretab7[10] = sphere32;
	spheretab7[11] = sphere33;
	spheretab7[12] = sphere34;
	spheretab7[13] = sphere35;
	spheretab7[14] = sphere36;
	spheretab7[15] = sphere37;
	spheretab7[16] = sphere38;
	spheretab7[17] = sphere39;
	spheretab7[18] = sphere40;
	spheretab7[19] = sphere41;
	spheretab7[20] = sphere42;
	spheretab7[21] = sphere43;
	spheretab7[22] = sphere44;
	spheretab7[23] = sphere45;
	spheretab7[24] = sphere46;
	spheretab7[25] = sphere47;
	spheretab7[26] = sphere48;
	spheretab7[27] = sphere49;
	spheretab7[28] = sphere50;
	spheretab7[29] = sphere51;
	spheretab7[30] = sphere52;
	spheretab7[31] = sphere53;
	spheretab7[32] = sphere54;
	spheretab7[33] = sphere55;
	spheretab7[34] = sphere56;
	spheretab7[35] = sphere57;
	spheretab7[36] = sphere58;
	spheretab7[37] = sphere59;
	spheretab7[38] = sphere60;
	spheretab7[39] = sphere61;
	spheretab7[40] = sphere62;
	spheretab7[41] = sphere63;
	spheretab7[42] = sphere64;
	spheretab7[43] = sphere65;
	spheretab7[44] = sphere66;
	spheretab7[45] = sphere67;
	spheretab7[46] = sphere68;
	spheretab7[47] = sphere69;
	spheretab7[48] = sphere70;
	spheretab7[49] = sphere71;
	spheretab7[50] = sphere72;
	spheretab7[51] = sphere73;
	spheretab7[52] = sphere74;
	spheretab7[53] = sphere75;
	
	//CH4 + 2O2 = CO2 + 2H2O
	markerRoot9 = new THREE.Group();
	scene.add(markerRoot9);
	
	markerRoot10 = new THREE.Group();
	scene.add(markerRoot10);
	let markerControls9 = new THREEx.ArMarkerControls(arToolkitContext, markerRoot9, {
		type: 'pattern', patternUrl: "data/patterntestou3.patt",
	})
	// pattern2C4H10 // pattern3H2 // pattern3O2 // pattern4Fe // pattern13O2 // patternC // patternCH4 // patternN2 // patternO2 // pattern2O2
	
	let markerControls10 = new THREEx.ArMarkerControls(arToolkitContext, markerRoot10, {
		type: 'pattern', patternUrl: "data/patterntestou4.patt",
	})
	
	// let floorMesh5 = new THREE.Mesh( floorGeometry, floorMaterial );
	// floorMesh5.rotation.x = -Math.PI/2;
	// floorMesh5.receiveShadow = true;
	// markerRoot9.add( floorMesh5 );
	
	
	//carbonne
	var sphereMaterial11 = new THREE.MeshPhongMaterial( { color: 0x000000, specular : 0x333333, reflectivity : 0.9 } );
	//oxygene
	var sphereMaterial12 = new THREE.MeshPhongMaterial( { color: 0xFF0000, specular : 0x333333, reflectivity : 0.1 } );
	//hydrogene
	var sphereMaterial13 = new THREE.MeshPhongMaterial( { color: 0xFFFFFF, specular : 0x333333, reflectivity : 0.5 } );
	
	//commence a 76
	
	//Carbone
	var sphere76 = new THREE.Mesh(new THREE.SphereGeometry( rayonCarbonne, 32, 32 ), sphereMaterial11 );
	//Hydrogene
	var sphere77 = new THREE.Mesh(new THREE.SphereGeometry( rayonHydrogene, 32, 32 ), sphereMaterial13 );
	var sphere78 = new THREE.Mesh(new THREE.SphereGeometry( rayonHydrogene, 32, 32 ), sphereMaterial13 );
	var sphere79 = new THREE.Mesh(new THREE.SphereGeometry( rayonHydrogene, 32, 32 ), sphereMaterial13 );
	var sphere80 = new THREE.Mesh(new THREE.SphereGeometry( rayonHydrogene, 32, 32 ), sphereMaterial13 );
	//Oxygene
	var sphere81 = new THREE.Mesh(new THREE.SphereGeometry( rayonOxygene, 32, 32 ), sphereMaterial12 );
	var sphere82 = new THREE.Mesh(new THREE.SphereGeometry( rayonOxygene, 32, 32 ), sphereMaterial12 );
	var sphere83 = new THREE.Mesh(new THREE.SphereGeometry( rayonOxygene, 32, 32 ), sphereMaterial12 );
	var sphere84 = new THREE.Mesh(new THREE.SphereGeometry( rayonOxygene, 32, 32 ), sphereMaterial12 );
	
	sphere76.position.x = 0.2;
	sphere76.position.y = 0.5;
	sphere76.position.z = -0.3;
	markerRoot9.add(sphere76);
	
	sphere77.position.x = sphere76.position.x;
	sphere77.position.y = sphere76.position.y + coupedecaleCarbHydro;
	sphere77.position.z = sphere76.position.z;
	markerRoot9.add(sphere77);
	
	sphere78.position.x = sphere76.position.x - coupedecaleCarbHydro;
	sphere78.position.y = sphere76.position.y - valeurSur;
	sphere78.position.z = sphere76.position.z - valeurSurx2;
	markerRoot9.add(sphere78);
	
	sphere79.position.x = sphere76.position.x + coupedecaleCarbHydro;
	sphere79.position.y = sphere76.position.y - valeurSur;
	sphere79.position.z = sphere76.position.z - valeurSurx2;
	markerRoot9.add(sphere79);
	
	sphere80.position.x = sphere76.position.x;
	sphere80.position.y = sphere76.position.y - valeurSur;
	sphere80.position.z = sphere76.position.z + coupedecaleCarbHydro;
	markerRoot9.add(sphere80);
	
	sphere81.position.x = 0.5;
	sphere81.position.y = 0.3;
	sphere81.position.z = 0.3;
	markerRoot10.add(sphere81);
	
	sphere82.position.x = sphere81.position.x + coupedecaleOxyOxy;
	sphere82.position.y = sphere81.position.y;
	sphere82.position.z = sphere81.position.z - valeurSur;
	markerRoot10.add(sphere82);
	
	sphere83.position.x = -0.5;
	sphere83.position.y = 1;
	sphere83.position.z = -0.2;
	markerRoot10.add(sphere83);
	
	sphere84.position.x = sphere83.position.x + coupedecaleOxyOxy;
	sphere84.position.y = sphere83.position.y;
	sphere84.position.z = sphere83.position.z;
	markerRoot10.add(sphere84);
	
	
	sphere76.castShadow = true;
	sphere77.castShadow = true;
	sphere78.castShadow = true;
	sphere79.castShadow = true;
	sphere80.castShadow = true;
	sphere81.castShadow = true;
	sphere82.castShadow = true;
	sphere83.castShadow = true;
	sphere84.castShadow = true;
	
	sphere76.receiveShadow = true;
	sphere77.receiveShadow = true;
	sphere78.receiveShadow = true;
	sphere79.receiveShadow = true;
	sphere80.receiveShadow = true;
	sphere81.receiveShadow = true;
	sphere82.receiveShadow = true;
	sphere83.receiveShadow = true;
	sphere84.receiveShadow = true;

	spheretab9[0] = sphere76;
	spheretab9[1] = sphere77;
	spheretab9[2] = sphere78;
	spheretab9[3] = sphere79;
	spheretab9[4] = sphere80;
	spheretab9[5] = sphere81;
	spheretab9[6] = sphere82;
	spheretab9[7] = sphere83;
	spheretab9[8] = sphere84;
}

function eaudio() {
	// let floorGeometry = new THREE.PlaneGeometry( 20,20 );
	// let floorMaterial = new THREE.ShadowMaterial();
	// floorMaterial.opacity = 0.3;
	// let floorMesh = new THREE.Mesh( floorGeometry, floorMaterial );
	// floorMesh.rotation.x = -Math.PI/2;
	// floorMesh.receiveShadow = true;
	// markerRoot7.add( floorMesh );
	
	//carbonne
	var sphereMaterial11 = new THREE.MeshPhongMaterial( { color: 0x000000, specular : 0x333333, reflectivity : 0.9 } );
	//oxygene
	var sphereMaterial12 = new THREE.MeshPhongMaterial( { color: 0xFF0000, specular : 0x333333, reflectivity : 0.1 } );
	//hydrogene
	var sphereMaterial13 = new THREE.MeshPhongMaterial( { color: 0xFFFFFF, specular : 0x333333, reflectivity : 0.5 } );
	
	//Carbone
	var sphere76 = new THREE.Mesh(new THREE.SphereGeometry( rayonCarbonne, 32, 32 ), sphereMaterial11 );
	//Hydrogene
	var sphere77 = new THREE.Mesh(new THREE.SphereGeometry( rayonHydrogene, 32, 32 ), sphereMaterial13 );
	var sphere78 = new THREE.Mesh(new THREE.SphereGeometry( rayonHydrogene, 32, 32 ), sphereMaterial13 );
	var sphere79 = new THREE.Mesh(new THREE.SphereGeometry( rayonHydrogene, 32, 32 ), sphereMaterial13 );
	var sphere80 = new THREE.Mesh(new THREE.SphereGeometry( rayonHydrogene, 32, 32 ), sphereMaterial13 );
	//Oxygene
	var sphere81 = new THREE.Mesh(new THREE.SphereGeometry( rayonOxygene, 32, 32 ), sphereMaterial12 );
	var sphere82 = new THREE.Mesh(new THREE.SphereGeometry( rayonOxygene, 32, 32 ), sphereMaterial12 );
	var sphere83 = new THREE.Mesh(new THREE.SphereGeometry( rayonOxygene, 32, 32 ), sphereMaterial12 );
	var sphere84 = new THREE.Mesh(new THREE.SphereGeometry( rayonOxygene, 32, 32 ), sphereMaterial12 );
	
	//CO2
	sphere76.position.x = 0.2;
	sphere76.position.y = 0.5;
	sphere76.position.z = -0.3;
	markerRoot9.add(sphere76);
	
	sphere81.position.x = sphere76.position.x + coupedecaleOxyCarb;
	sphere81.position.y = sphere76.position.y - valeurSur;
	sphere81.position.z = sphere76.position.z;
	markerRoot9.add(sphere81);
	
	sphere82.position.x = sphere76.position.x - coupedecaleOxyCarb;
	sphere82.position.y = sphere76.position.y - valeurSur;
	sphere82.position.z = sphere76.position.z;
	markerRoot9.add(sphere82);
	
	//H2O
	sphere83.position.x = -0.5;
	sphere83.position.y = 0.8;
	sphere83.position.z = 0;
	markerRoot9.add(sphere83);
	
	sphere77.position.x = sphere83.position.x + coupedecaleOxyHydro;
	sphere77.position.y = sphere83.position.y - valeurSur;
	sphere77.position.z = sphere83.position.z;
	markerRoot9.add(sphere77);
	
	sphere78.position.x = sphere83.position.x - coupedecaleOxyHydro;
	sphere78.position.y = sphere83.position.y - valeurSur;
	sphere78.position.z = sphere83.position.z;
	markerRoot9.add(sphere78);
	
	
	
	//H2O
	sphere84.position.x = -1;
	sphere84.position.y = 0.6;
	sphere84.position.z = -0.5;
	markerRoot9.add(sphere84);
	
	sphere79.position.x = sphere84.position.x;
	sphere79.position.y = sphere84.position.y - valeurSur;
	sphere79.position.z = sphere84.position.z + coupedecaleOxyHydro;
	markerRoot9.add(sphere79);
	
	sphere80.position.x = sphere84.position.x;
	sphere80.position.y = sphere84.position.y - valeurSur;
	sphere80.position.z = sphere84.position.z - coupedecaleOxyHydro;
	markerRoot9.add(sphere80);
	
	
	
	
	sphere76.castShadow = true;
	sphere77.castShadow = true;
	sphere78.castShadow = true;
	sphere79.castShadow = true;
	sphere80.castShadow = true;
	sphere81.castShadow = true;
	sphere82.castShadow = true;
	sphere83.castShadow = true;
	sphere84.castShadow = true;
	
	sphere76.receiveShadow = true;
	sphere77.receiveShadow = true;
	sphere78.receiveShadow = true;
	sphere79.receiveShadow = true;
	sphere80.receiveShadow = true;
	sphere81.receiveShadow = true;
	sphere82.receiveShadow = true;
	sphere83.receiveShadow = true;
	sphere84.receiveShadow = true;

	spheretab10[0] = sphere76;
	spheretab10[1] = sphere77;
	spheretab10[2] = sphere78;
	spheretab10[3] = sphere79;
	spheretab10[4] = sphere80;
	spheretab10[5] = sphere81;
	spheretab10[6] = sphere82;
	spheretab10[7] = sphere83;
	spheretab10[8] = sphere84;
	
	console.log("eaudio lancé");
	
}

function dioeau() {
	// let floorGeometry = new THREE.PlaneGeometry( 20,20 );
	// let floorMaterial = new THREE.ShadowMaterial();
	// floorMaterial.opacity = 0.3;
	// let floorMesh = new THREE.Mesh( floorGeometry, floorMaterial );
	// floorMesh.rotation.x = -Math.PI/2;
	// floorMesh.receiveShadow = true;
	// markerRoot7.add( floorMesh );
	
	
	//carbonne
	var sphereMaterial8 = new THREE.MeshPhongMaterial( { color: 0x000000, specular : 0x333333, reflectivity : 0.9 } );
	//oxygene
	var sphereMaterial9 = new THREE.MeshPhongMaterial( { color: 0xFF0000, specular : 0x333333, reflectivity : 0.1 } );
	//hydrogene
	var sphereMaterial10 = new THREE.MeshPhongMaterial( { color: 0xFFFFFF, specular : 0x333333, reflectivity : 0.1 } );
	
	//commence a 21
	
	
	
	//H / Hydrogene
	var sphere22 = new THREE.Mesh(new THREE.SphereGeometry( rayonHydrogene, 32, 32 ), sphereMaterial10 );
	var sphere23 = new THREE.Mesh(new THREE.SphereGeometry( rayonHydrogene, 32, 32 ), sphereMaterial10 );
	var sphere24 = new THREE.Mesh(new THREE.SphereGeometry( rayonHydrogene, 32, 32 ), sphereMaterial10 );
	var sphere25 = new THREE.Mesh(new THREE.SphereGeometry( rayonHydrogene, 32, 32 ), sphereMaterial10 );
	var sphere26 = new THREE.Mesh(new THREE.SphereGeometry( rayonHydrogene, 32, 32 ), sphereMaterial10 );
	var sphere27 = new THREE.Mesh(new THREE.SphereGeometry( rayonHydrogene, 32, 32 ), sphereMaterial10 );
	var sphere28 = new THREE.Mesh(new THREE.SphereGeometry( rayonHydrogene, 32, 32 ), sphereMaterial10 );
	var sphere29 = new THREE.Mesh(new THREE.SphereGeometry( rayonHydrogene, 32, 32 ), sphereMaterial10 );
	var sphere30 = new THREE.Mesh(new THREE.SphereGeometry( rayonHydrogene, 32, 32 ), sphereMaterial10 );
	var sphere31 = new THREE.Mesh(new THREE.SphereGeometry( rayonHydrogene, 32, 32 ), sphereMaterial10 );
	var sphere32 = new THREE.Mesh(new THREE.SphereGeometry( rayonHydrogene, 32, 32 ), sphereMaterial10 );
	var sphere33 = new THREE.Mesh(new THREE.SphereGeometry( rayonHydrogene, 32, 32 ), sphereMaterial10 );
	var sphere34 = new THREE.Mesh(new THREE.SphereGeometry( rayonHydrogene, 32, 32 ), sphereMaterial10 );
	var sphere35 = new THREE.Mesh(new THREE.SphereGeometry( rayonHydrogene, 32, 32 ), sphereMaterial10 );
	var sphere36 = new THREE.Mesh(new THREE.SphereGeometry( rayonHydrogene, 32, 32 ), sphereMaterial10 );
	var sphere37 = new THREE.Mesh(new THREE.SphereGeometry( rayonHydrogene, 32, 32 ), sphereMaterial10 );
	var sphere38 = new THREE.Mesh(new THREE.SphereGeometry( rayonHydrogene, 32, 32 ), sphereMaterial10 );
	var sphere39 = new THREE.Mesh(new THREE.SphereGeometry( rayonHydrogene, 32, 32 ), sphereMaterial10 );
	var sphere40 = new THREE.Mesh(new THREE.SphereGeometry( rayonHydrogene, 32, 32 ), sphereMaterial10 );
	var sphere41 = new THREE.Mesh(new THREE.SphereGeometry( rayonHydrogene, 32, 32 ), sphereMaterial10 );
	
	// O / Oxygene
	var sphere42 = new THREE.Mesh(new THREE.SphereGeometry( rayonOxygene, 32, 32 ), sphereMaterial9 );
	var sphere43 = new THREE.Mesh(new THREE.SphereGeometry( rayonOxygene, 32, 32 ), sphereMaterial9 );
	var sphere44 = new THREE.Mesh(new THREE.SphereGeometry( rayonOxygene, 32, 32 ), sphereMaterial9 );
	var sphere45 = new THREE.Mesh(new THREE.SphereGeometry( rayonOxygene, 32, 32 ), sphereMaterial9 );
	var sphere46 = new THREE.Mesh(new THREE.SphereGeometry( rayonOxygene, 32, 32 ), sphereMaterial9 );
	var sphere47 = new THREE.Mesh(new THREE.SphereGeometry( rayonOxygene, 32, 32 ), sphereMaterial9 );
	var sphere48 = new THREE.Mesh(new THREE.SphereGeometry( rayonOxygene, 32, 32 ), sphereMaterial9 );
	var sphere49 = new THREE.Mesh(new THREE.SphereGeometry( rayonOxygene, 32, 32 ), sphereMaterial9 );
	var sphere50 = new THREE.Mesh(new THREE.SphereGeometry( rayonOxygene, 32, 32 ), sphereMaterial9 );
	var sphere51 = new THREE.Mesh(new THREE.SphereGeometry( rayonOxygene, 32, 32 ), sphereMaterial9 );
	var sphere52 = new THREE.Mesh(new THREE.SphereGeometry( rayonOxygene, 32, 32 ), sphereMaterial9 );
	var sphere53 = new THREE.Mesh(new THREE.SphereGeometry( rayonOxygene, 32, 32 ), sphereMaterial9 );
	var sphere54 = new THREE.Mesh(new THREE.SphereGeometry( rayonOxygene, 32, 32 ), sphereMaterial9 );
	var sphere55 = new THREE.Mesh(new THREE.SphereGeometry( rayonOxygene, 32, 32 ), sphereMaterial9 );
	var sphere56 = new THREE.Mesh(new THREE.SphereGeometry( rayonOxygene, 32, 32 ), sphereMaterial9 );
	var sphere57 = new THREE.Mesh(new THREE.SphereGeometry( rayonOxygene, 32, 32 ), sphereMaterial9 );
	var sphere58 = new THREE.Mesh(new THREE.SphereGeometry( rayonOxygene, 32, 32 ), sphereMaterial9 );
	var sphere59 = new THREE.Mesh(new THREE.SphereGeometry( rayonOxygene, 32, 32 ), sphereMaterial9 );
	var sphere60 = new THREE.Mesh(new THREE.SphereGeometry( rayonOxygene, 32, 32 ), sphereMaterial9 );
	var sphere61 = new THREE.Mesh(new THREE.SphereGeometry( rayonOxygene, 32, 32 ), sphereMaterial9 );
	var sphere62 = new THREE.Mesh(new THREE.SphereGeometry( rayonOxygene, 32, 32 ), sphereMaterial9 );
	var sphere63 = new THREE.Mesh(new THREE.SphereGeometry( rayonOxygene, 32, 32 ), sphereMaterial9 );
	var sphere64 = new THREE.Mesh(new THREE.SphereGeometry( rayonOxygene, 32, 32 ), sphereMaterial9 );
	var sphere65 = new THREE.Mesh(new THREE.SphereGeometry( rayonOxygene, 32, 32 ), sphereMaterial9 );
	var sphere66 = new THREE.Mesh(new THREE.SphereGeometry( rayonOxygene, 32, 32 ), sphereMaterial9 );
	var sphere67 = new THREE.Mesh(new THREE.SphereGeometry( rayonOxygene, 32, 32 ), sphereMaterial9 );
	
	//C / Carbone
	var sphere68 = new THREE.Mesh(new THREE.SphereGeometry( rayonCarbonne, 32, 32 ), sphereMaterial8 );
	var sphere69 = new THREE.Mesh(new THREE.SphereGeometry( rayonCarbonne, 32, 32 ), sphereMaterial8 );
	var sphere70 = new THREE.Mesh(new THREE.SphereGeometry( rayonCarbonne, 32, 32 ), sphereMaterial8 );
	var sphere71 = new THREE.Mesh(new THREE.SphereGeometry( rayonCarbonne, 32, 32 ), sphereMaterial8 );
	var sphere72 = new THREE.Mesh(new THREE.SphereGeometry( rayonCarbonne, 32, 32 ), sphereMaterial8 );
	var sphere73 = new THREE.Mesh(new THREE.SphereGeometry( rayonCarbonne, 32, 32 ), sphereMaterial8 );
	var sphere74 = new THREE.Mesh(new THREE.SphereGeometry( rayonCarbonne, 32, 32 ), sphereMaterial8 );
	var sphere75 = new THREE.Mesh(new THREE.SphereGeometry( rayonCarbonne, 32, 32 ), sphereMaterial8 );
	
	
	// CO2 1
	//C 1
	sphere68.position.x = -1;
	sphere68.position.y = 0;
	sphere68.position.z = 0;
	markerRoot7.add(sphere68);
	//O2 1
	sphere42.position.x = sphere68.position.x + coupedecaleOxyCarb;
	sphere42.position.y = sphere68.position.y - valeurSurx2;
	sphere42.position.z = sphere68.position.z;
	markerRoot7.add(sphere42);
	
	sphere43.position.x = sphere68.position.x - coupedecaleOxyCarb;
	sphere43.position.y = sphere68.position.y - valeurSurx2;
	sphere43.position.z = sphere68.position.z;
	markerRoot7.add(sphere43);
	
	
	//CO2 2
	//C 2
	sphere69.position.x = 1;
	sphere69.position.y = 0;
	sphere69.position.z = 0;
	markerRoot7.add(sphere69);
	//O2 2
	sphere44.position.x = sphere69.position.x + coupedecaleOxyCarb;
	sphere44.position.y = sphere69.position.y - valeurSurx2;
	sphere44.position.z = sphere69.position.z;
	markerRoot7.add(sphere44);
	
	sphere45.position.x = sphere69.position.x - coupedecaleOxyCarb;
	sphere45.position.y = sphere69.position.y - valeurSurx2;
	sphere45.position.z = sphere69.position.z;
	markerRoot7.add(sphere45);
	
	
	//CO2 3
	//C 1
	sphere70.position.x = 0;
	sphere70.position.y = 0;
	sphere70.position.z = -1;
	markerRoot7.add(sphere70);
	//O2 3
	sphere46.position.x = sphere70.position.x + coupedecaleOxyCarb;
	sphere46.position.y = sphere70.position.y - valeurSurx2;
	sphere46.position.z = sphere70.position.z;
	markerRoot7.add(sphere46);
	
	sphere47.position.x = sphere70.position.x - coupedecaleOxyCarb;
	sphere47.position.y = sphere70.position.y - valeurSurx2;
	sphere47.position.z = sphere70.position.z;
	markerRoot7.add(sphere47);
	
	
	
	//CO2 4
	//C 1
	sphere71.position.x = 0;
	sphere71.position.y = 0;
	sphere71.position.z = 1;
	markerRoot7.add(sphere71);
	//O2 4
	sphere48.position.x = sphere71.position.x + coupedecaleOxyCarb;
	sphere48.position.y = sphere71.position.y - valeurSurx2;
	sphere48.position.z = sphere71.position.z;
	markerRoot7.add(sphere48);
	
	sphere49.position.x = sphere71.position.x - coupedecaleOxyCarb;
	sphere49.position.y = sphere71.position.y - valeurSurx2;
	sphere49.position.z = sphere71.position.z;
	markerRoot7.add(sphere49);
	
	
	//CO2 5
	//C 2
	sphere72.position.x = -0.5;
	sphere72.position.y = 0;
	sphere72.position.z = 0.5;
	markerRoot7.add(sphere72);
	//O2 5
	sphere50.position.x = sphere72.position.x + coupedecaleOxyCarb;
	sphere50.position.y = sphere72.position.y - valeurSurx2;
	sphere50.position.z = sphere72.position.z;
	markerRoot7.add(sphere50);
	
	sphere51.position.x = sphere72.position.x - coupedecaleOxyCarb;
	sphere51.position.y = sphere72.position.y - valeurSurx2;
	sphere51.position.z = sphere72.position.z;
	markerRoot7.add(sphere51);
	
	
	//CO2 6
	//C 2
	sphere73.position.x = -0.5;
	sphere73.position.y = 0;
	sphere73.position.z = -0.5;
	markerRoot7.add(sphere73);
	//O2 6
	sphere52.position.x = sphere73.position.x + coupedecaleOxyCarb;
	sphere52.position.y = sphere73.position.y - valeurSurx2;
	sphere52.position.z = sphere73.position.z;
	markerRoot7.add(sphere52);
	
	sphere53.position.x = sphere73.position.x - coupedecaleOxyCarb;
	sphere53.position.y = sphere73.position.y - valeurSurx2;
	sphere53.position.z = sphere73.position.z;
	markerRoot7.add(sphere53);
	
	
	//CO2 7
	//C 2
	sphere74.position.x = 0.5;
	sphere74.position.y = 0;
	sphere74.position.z = 0.5;
	markerRoot7.add(sphere74);
	//O2 7
	sphere54.position.x = sphere74.position.x + coupedecaleOxyCarb;
	sphere54.position.y = sphere74.position.y - valeurSurx2;
	sphere54.position.z = sphere74.position.z;
	markerRoot7.add(sphere54);
	
	sphere55.position.x = sphere74.position.x - coupedecaleOxyCarb;
	sphere55.position.y = sphere74.position.y - valeurSurx2;
	sphere55.position.z = sphere74.position.z;
	markerRoot7.add(sphere55);
	
	
	//CO2 8
	//C 2
	sphere75.position.x = 0.5;
	sphere75.position.y = 0;
	sphere75.position.z = -0.5;
	markerRoot7.add(sphere75);
	//O2 8
	sphere56.position.x = sphere75.position.x + coupedecaleOxyCarb;
	sphere56.position.y = sphere75.position.y - valeurSurx2;
	sphere56.position.z = sphere75.position.z;
	markerRoot7.add(sphere56);
	
	sphere57.position.x = sphere75.position.x - coupedecaleOxyCarb;
	sphere57.position.y = sphere75.position.y - valeurSurx2;
	sphere57.position.z = sphere75.position.z;
	markerRoot7.add(sphere57);
	
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	// H2O 1
	// O 1
	sphere58.position.x = 0;
	sphere58.position.y = 0;
	sphere58.position.z = 0;
	markerRoot7.add(sphere58);
	// H 1
	sphere22.position.x = sphere58.position.x + coupedecaleOxyHydro;
	sphere22.position.y = sphere58.position.y - valeurSur;
	sphere22.position.z = sphere58.position.z;
	markerRoot7.add(sphere22);
	// H 1
	sphere23.position.x = sphere58.position.x - coupedecaleOxyHydro;
	sphere23.position.y = sphere58.position.y - valeurSur;
	sphere23.position.z = sphere58.position.z;
	markerRoot7.add(sphere23);
	
	
	//H2O 2
	//O2
	sphere59.position.x = 1 ;
	sphere59.position.y = 0 ;
	sphere59.position.z = 1;
	markerRoot7.add(sphere59);
	//H 2
	sphere24.position.x = sphere59.position.x + coupedecaleOxyHydro;
	sphere24.position.y = sphere59.position.y - valeurSur;
	sphere24.position.z = sphere59.position.z;
	markerRoot7.add(sphere24);
	//H 2
	sphere25.position.x = sphere59.position.x - coupedecaleOxyHydro;
	sphere25.position.y = sphere59.position.y - valeurSur;
	sphere25.position.z = sphere59.position.z;
	markerRoot7.add(sphere25);
	
	
	//H2O 3
	//O 3
	sphere60.position.x = 1;
	sphere60.position.y = 0;
	sphere60.position.z = -1;
	markerRoot7.add(sphere60);
	//H 3
	sphere26.position.x = sphere60.position.x + coupedecaleOxyHydro;
	sphere26.position.y = sphere60.position.y - valeurSur;
	sphere26.position.z = sphere60.position.z;
	markerRoot7.add(sphere26);
	//H 3
	sphere27.position.x = sphere60.position.x - coupedecaleOxyHydro;
	sphere27.position.y = sphere60.position.y - valeurSur;
	sphere27.position.z = sphere60.position.z ;
	markerRoot7.add(sphere27);
	
	
	//H2O 4
	//O 4
	sphere61.position.x = -1;
	sphere61.position.y = 0;
	sphere61.position.z = 1;
	markerRoot7.add(sphere61);
	//H 4
	sphere28.position.x = sphere61.position.x + coupedecaleOxyHydro;
	sphere28.position.y = sphere61.position.y - valeurSur;
	sphere28.position.z = sphere61.position.z;
	markerRoot7.add(sphere28);
	//H 4
	sphere29.position.x = sphere61.position.x - coupedecaleOxyHydro;
	sphere29.position.y = sphere61.position.y - valeurSur;
	sphere29.position.z = sphere61.position.z;
	markerRoot7.add(sphere29);
	
	
	//H2O 5
	//O 5
	sphere62.position.x = -1;
	sphere62.position.y = 0;
	sphere62.position.z = -1;
	markerRoot7.add(sphere62);
	//H 5
	sphere30.position.x = sphere62.position.x + coupedecaleOxyHydro;
	sphere30.position.y = sphere62.position.y - valeurSur;
	sphere30.position.z = sphere62.position.z;
	markerRoot7.add(sphere30);
	//H 5
	sphere31.position.x = sphere62.position.x - coupedecaleOxyHydro;
	sphere31.position.y = sphere62.position.y - valeurSur;
	sphere31.position.z = sphere62.position.z;
	markerRoot7.add(sphere31);
	
	
	//H2O 6
	//O 6
	sphere63.position.x = 0;
	sphere63.position.y = 0.5;
	sphere63.position.z = -0.5;
	markerRoot7.add(sphere63);
	//H 6
	sphere32.position.x = sphere63.position.x + coupedecaleOxyHydro;
	sphere32.position.y = sphere63.position.y - valeurSur;
	sphere32.position.z = sphere63.position.z;
	markerRoot7.add(sphere32);
	//H 6
	sphere33.position.x = sphere63.position.x - coupedecaleOxyHydro;
	sphere33.position.y = sphere63.position.y - valeurSur;
	sphere33.position.z = sphere63.position.z;
	markerRoot7.add(sphere33);
	
	
	//H2O 7
	//O2 7
	sphere64.position.x = 0;
	sphere64.position.y = 0.5;
	sphere64.position.z = 0.5;
	markerRoot7.add(sphere64);
	//H 7
	sphere34.position.x = sphere64.position.x + coupedecaleOxyHydro;
	sphere34.position.y = sphere64.position.y - valeurSur;
	sphere34.position.z = sphere64.position.z;
	markerRoot7.add(sphere34);
	//H 7
	sphere35.position.x = sphere64.position.x - coupedecaleOxyHydro;
	sphere35.position.y = sphere64.position.y - valeurSur;
	sphere35.position.z = sphere64.position.z;
	markerRoot7.add(sphere35);
	
	
	//H2O 8
	//O2 8
	sphere65.position.x = -0.5;
	sphere65.position.y = 0.5;
	sphere65.position.z = 0;
	markerRoot7.add(sphere65);
	//H 8
	sphere36.position.x = sphere65.position.x + coupedecaleOxyHydro;
	sphere36.position.y = sphere65.position.y - valeurSur;
	sphere36.position.z = sphere65.position.z;
	markerRoot7.add(sphere36);
	//H 8
	sphere37.position.x = sphere65.position.x - coupedecaleOxyHydro;
	sphere37.position.y = sphere65.position.y - valeurSur;
	sphere37.position.z = sphere65.position.z;
	markerRoot7.add(sphere37);
	
	
	//H2O 9
	//O2 9
	sphere66.position.x = 0.5;
	sphere66.position.y = 0.5;
	sphere66.position.z = 0;
	markerRoot7.add(sphere66);
	//H 9
	sphere38.position.x = sphere66.position.x + coupedecaleOxyHydro;
	sphere38.position.y = sphere66.position.y - valeurSur;
	sphere38.position.z = sphere66.position.z;
	markerRoot7.add(sphere38);
	//H 9
	sphere39.position.x = sphere66.position.x - coupedecaleOxyHydro;
	sphere39.position.y = sphere66.position.y - valeurSur;
	sphere39.position.z = sphere66.position.z;
	markerRoot7.add(sphere39);
	
	
	//H2O 10
	//O 10
	sphere67.position.x = 0;
	sphere67.position.y = 1;
	sphere67.position.z = 0;
	markerRoot7.add(sphere67);
	//H 10
	sphere40.position.x = sphere67.position.x + coupedecaleOxyHydro;
	sphere40.position.y = sphere67.position.y - valeurSur;
	sphere40.position.z = sphere67.position.z;
	markerRoot7.add(sphere40);
	//H 10
	sphere41.position.x = sphere67.position.x - coupedecaleOxyHydro;
	sphere41.position.y = sphere67.position.y - valeurSur;
	sphere41.position.z = sphere67.position.z;
	markerRoot7.add(sphere41);
	
	
	
	//22 - 75
	sphere22.castShadow = true;
	sphere23.castShadow = true;
	sphere24.castShadow = true;
	sphere25.castShadow = true;
	sphere26.castShadow = true;
	sphere27.castShadow = true;
	sphere28.castShadow = true;
	sphere29.castShadow = true;
	sphere30.castShadow = true;
	sphere31.castShadow = true;
	sphere32.castShadow = true;
	sphere33.castShadow = true;
	sphere34.castShadow = true;
	sphere35.castShadow = true;
	sphere36.castShadow = true;
	sphere37.castShadow = true;
	sphere38.castShadow = true;
	sphere39.castShadow = true;
	sphere40.castShadow = true;
	sphere41.castShadow = true;
	sphere42.castShadow = true;
	sphere43.castShadow = true;
	sphere44.castShadow = true;
	sphere45.castShadow = true;
	sphere46.castShadow = true;
	sphere47.castShadow = true;
	sphere48.castShadow = true;
	sphere49.castShadow = true;
	sphere50.castShadow = true;
	sphere51.castShadow = true;
	sphere52.castShadow = true;
	sphere53.castShadow = true;
	sphere54.castShadow = true;
	sphere55.castShadow = true;
	sphere56.castShadow = true;
	sphere57.castShadow = true;
	sphere58.castShadow = true;
	sphere59.castShadow = true;
	sphere60.castShadow = true;
	sphere61.castShadow = true;
	sphere62.castShadow = true;
	sphere63.castShadow = true;
	sphere64.castShadow = true;
	sphere65.castShadow = true;
	sphere66.castShadow = true;
	sphere67.castShadow = true;
	sphere68.castShadow = true;
	sphere69.castShadow = true;
	sphere70.castShadow = true;
	sphere71.castShadow = true;
	sphere72.castShadow = true;
	sphere73.castShadow = true;
	sphere74.castShadow = true;
	sphere75.castShadow = true;
	
	sphere22.receiveShadow = true;
	sphere23.receiveShadow = true;
	sphere24.receiveShadow = true;
	sphere25.receiveShadow = true;
	sphere26.receiveShadow = true;
	sphere27.receiveShadow = true;
	sphere28.receiveShadow = true;
	sphere29.receiveShadow = true;
	sphere30.receiveShadow = true;
	sphere31.receiveShadow = true;
	sphere32.receiveShadow = true;
	sphere33.receiveShadow = true;
	sphere34.receiveShadow = true;
	sphere35.receiveShadow = true;
	sphere36.receiveShadow = true;
	sphere37.receiveShadow = true;
	sphere38.receiveShadow = true;
	sphere39.receiveShadow = true;
	sphere40.receiveShadow = true;
	sphere41.receiveShadow = true;
	sphere42.receiveShadow = true;
	sphere43.receiveShadow = true;
	sphere44.receiveShadow = true;
	sphere45.receiveShadow = true;
	sphere46.receiveShadow = true;
	sphere47.receiveShadow = true;
	sphere48.receiveShadow = true;
	sphere49.receiveShadow = true;
	sphere50.receiveShadow = true;
	sphere51.receiveShadow = true;
	sphere52.receiveShadow = true;
	sphere53.receiveShadow = true;
	sphere54.receiveShadow = true;
	sphere55.receiveShadow = true;
	sphere56.receiveShadow = true;
	sphere57.receiveShadow = true;
	sphere58.receiveShadow = true;
	sphere59.receiveShadow = true;
	sphere60.receiveShadow = true;
	sphere61.receiveShadow = true;
	sphere62.receiveShadow = true;
	sphere63.receiveShadow = true;
	sphere64.receiveShadow = true;
	sphere65.receiveShadow = true;
	sphere66.receiveShadow = true;
	sphere67.receiveShadow = true;
	sphere68.receiveShadow = true;
	sphere69.receiveShadow = true;
	sphere70.receiveShadow = true;
	sphere71.receiveShadow = true;
	sphere72.receiveShadow = true;
	sphere73.receiveShadow = true;
	sphere74.receiveShadow = true;
	sphere75.receiveShadow = true;

	
	

	spheretab8[0] = sphere22;
	spheretab8[1] = sphere23;
	spheretab8[2] = sphere24;
	spheretab8[3] = sphere25;
	spheretab8[4] = sphere26;
	spheretab8[5] = sphere27;
	spheretab8[6] = sphere28;
	spheretab8[7] = sphere29;
	spheretab8[8] = sphere30;
	spheretab8[9] = sphere31;
	spheretab8[10] = sphere32;
	spheretab8[11] = sphere33;
	spheretab8[12] = sphere34;
	spheretab8[13] = sphere35;
	spheretab8[14] = sphere36;
	spheretab8[15] = sphere37;
	spheretab8[16] = sphere38;
	spheretab8[17] = sphere39;
	spheretab8[18] = sphere40;
	spheretab8[19] = sphere41;
	spheretab8[20] = sphere42;
	spheretab8[21] = sphere43;
	spheretab8[22] = sphere44;
	spheretab8[23] = sphere45;
	spheretab8[24] = sphere46;
	spheretab8[25] = sphere47;
	spheretab8[26] = sphere48;
	spheretab8[27] = sphere49;
	spheretab8[28] = sphere50;
	spheretab8[29] = sphere51;
	spheretab8[30] = sphere52;
	spheretab8[31] = sphere53;
	spheretab8[32] = sphere54;
	spheretab8[33] = sphere55;
	spheretab8[34] = sphere56;
	spheretab8[35] = sphere57;
	spheretab8[36] = sphere58;
	spheretab8[37] = sphere59;
	spheretab8[38] = sphere60;
	spheretab8[39] = sphere61;
	spheretab8[40] = sphere62;
	spheretab8[41] = sphere63;
	spheretab8[42] = sphere64;
	spheretab8[43] = sphere65;
	spheretab8[44] = sphere66;
	spheretab8[45] = sphere67;
	spheretab8[46] = sphere68;
	spheretab8[47] = sphere69;
	spheretab8[48] = sphere70;
	spheretab8[49] = sphere71;
	spheretab8[50] = sphere72;
	spheretab8[51] = sphere73;
	spheretab8[52] = sphere74;
	spheretab8[53] = sphere75;
	
}

function dioxydedecarbone() {
	
	
	

	
	var sphereMaterial1 = new THREE.MeshPhongMaterial( { color: 0x0000FF, specular : 0x222222 } );
	var sphereMaterial2 = new THREE.MeshPhongMaterial( { color: 0xFF0000, specular : 0x333333, reflectivity : 0.9 } );
	var sphereMaterial3 = new THREE.MeshPhongMaterial( { color: 0x000000, specular : 0x333333, reflectivity : 0.1} );
	// sphereMaterial3.specular = 0x222222; 
	
	var sphere4 = new THREE.Mesh(new THREE.SphereGeometry( rayonOxygene, 32, 32 ), sphereMaterial2 );
	var sphere5 = new THREE.Mesh(new THREE.SphereGeometry( rayonOxygene, 32, 32 ), sphereMaterial2 );
	var sphere6 = new THREE.Mesh(new THREE.SphereGeometry( rayonCarbonne, 32, 32 ), sphereMaterial3 );
	
	sphere6.position.x = 0;
	sphere6.position.y = 1;
	sphere6.position.z = 0;
	markerRoot1.add(sphere6)

	sphere4.position.x = sphere6.position.x - coupedecaleOxyCarb;
	sphere4.position.y = sphere6.position.y - valeurSurx2;
	sphere4.position.z = sphere6.position.z;
	markerRoot1.add(sphere4);
	

	sphere5.position.x = sphere6.position.x + coupedecaleOxyCarb;
	sphere5.position.y = sphere6.position.y - valeurSurx2;
	sphere5.position.z = sphere6.position.z;
	markerRoot1.add(sphere5);
	 
	
	
	
	sphere4.castShadow = true;
	sphere5.castShadow = true;
	sphere6.castShadow = true;
	
	sphere4.receiveShadow = true;
	sphere5.receiveShadow = true;
	sphere6.receiveShadow = true;
	
	spheretab2[0] = sphere4;
	spheretab2[1] = sphere5;
	spheretab2[2] = sphere6;
	
	markerRoot1.traverse( function ( object ) {
		if ( object instanceof THREE.Mesh ) {
			object.castShadow = true;
			object.receiveShadow = true;
		}
	} );

}

function ammoniac() {
	// let floorGeometry = new THREE.PlaneGeometry( 20,20 );
	// let floorMaterial = new THREE.ShadowMaterial();
	// floorMaterial.opacity = 0.3;
	// let floorMesh = new THREE.Mesh( floorGeometry, floorMaterial );
	// floorMesh.rotation.x = -Math.PI/2;
	// floorMesh.receiveShadow = true;
	// markerRoot3.add( floorMesh );
	
	//azote
	var sphereMaterial4 = new THREE.MeshPhongMaterial( { color: 0x318CE7 } );
	//hydrogène
	var sphereMaterial5 = new THREE.MeshPhongMaterial( { color: 0xFFFFFF } );
	
	
	var sphere4 = new THREE.Mesh(new THREE.SphereGeometry( rayonAzote, 32, 32 ), sphereMaterial4 );
	var sphere5 = new THREE.Mesh(new THREE.SphereGeometry( rayonAzote, 32, 32 ), sphereMaterial4 );
	
	var sphere6 = new THREE.Mesh(new THREE.SphereGeometry( rayonHydrogene, 32, 32 ), sphereMaterial5 );
	var sphere7 = new THREE.Mesh(new THREE.SphereGeometry( rayonHydrogene, 32, 32 ), sphereMaterial5 );
	var sphere8 = new THREE.Mesh(new THREE.SphereGeometry( rayonHydrogene, 32, 32 ), sphereMaterial5 );
	var sphere9 = new THREE.Mesh(new THREE.SphereGeometry( rayonHydrogene, 32, 32 ), sphereMaterial5 );
	var sphere10 = new THREE.Mesh(new THREE.SphereGeometry( rayonHydrogene, 32, 32 ), sphereMaterial5 );
	var sphere11 = new THREE.Mesh(new THREE.SphereGeometry( rayonHydrogene, 32, 32 ), sphereMaterial5 );
	
										//azote
	sphere4.position.x = -0.5;
	sphere4.position.y = 1;
	sphere4.position.z = 0;
	markerRoot3.add(sphere4);
	
	sphere5.position.x = 0.1;
	sphere5.position.y = 0.5;
	sphere5.position.z = 0;
	markerRoot3.add(sphere5);
	
										//hydrogène
	sphere6.position.x = sphere4.position.x + coupedecaleAzoHydro;
	sphere6.position.y = sphere4.position.y - valeurSurx3;
	sphere6.position.z = sphere4.position.z - valeurSurx2;
	markerRoot3.add(sphere6)
	
	sphere7.position.x = sphere4.position.x - coupedecaleAzoHydro;
	sphere7.position.y = sphere4.position.y - valeurSurx3;
	sphere7.position.z = sphere4.position.z - valeurSurx2;
	markerRoot3.add(sphere7)
	
	sphere8.position.x = sphere4.position.x;
	sphere8.position.y = sphere4.position.y - valeurSurx3;
	sphere8.position.z = sphere4.position.z + coupedecaleAzoHydro;
	markerRoot3.add(sphere8)
	
	sphere9.position.x = sphere5.position.x + coupedecaleAzoHydro;
	sphere9.position.y = sphere5.position.y - valeurSurx3;
	sphere9.position.z = sphere5.position.z - valeurSurx2;
	markerRoot3.add(sphere9)
	
	sphere10.position.x = sphere5.position.x - coupedecaleAzoHydro;
	sphere10.position.y = sphere5.position.y - valeurSurx3;
	sphere10.position.z = sphere5.position.z - valeurSurx2;
	markerRoot3.add(sphere10)
	
	sphere11.position.x = sphere5.position.x;
	sphere11.position.y = sphere5.position.y - valeurSurx3;
	sphere11.position.z = sphere5.position.z + coupedecaleAzoHydro;
	markerRoot3.add(sphere11)
	
	sphere4.castShadow = true;
	sphere5.castShadow = true;
	sphere6.castShadow = true;
	sphere7.castShadow = true;
	sphere8.castShadow = true;
	sphere9.castShadow = true;
	sphere10.castShadow = true;
	sphere11.castShadow = true;

	
	sphere4.receiveShadow = true;
	sphere5.receiveShadow = true;
	sphere6.receiveShadow = true;
	sphere7.receiveShadow = true;
	sphere8.receiveShadow = true;
	sphere9.receiveShadow = true;
	sphere10.receiveShadow = true;
	sphere11.receiveShadow = true;

	spheretab4[0] = sphere4;
	spheretab4[1] = sphere5;
	spheretab4[2] = sphere6;
	spheretab4[3] = sphere7;
	spheretab4[4] = sphere8;
	spheretab4[5] = sphere9;
	spheretab4[6] = sphere10;
	spheretab4[7] = sphere11;
}


function oxydeferrique() {
	// let floorGeometry = new THREE.PlaneGeometry( 20,20 );
	// let floorMaterial = new THREE.ShadowMaterial();
	// floorMaterial.opacity = 0.3;
	// let floorMesh = new THREE.Mesh( floorGeometry, floorMaterial );
	// floorMesh.rotation.x = -Math.PI/2;
	// floorMesh.receiveShadow = true;
	// markerRoot5.add( floorMesh );
	
	
	//fer
	var sphereMaterial6 = new THREE.MeshPhongMaterial( { color: 0x7F7F7F, specular : 0x333333, reflectivity : 0.9 } );
	//oxygene
	var sphereMaterial7 = new THREE.MeshPhongMaterial( { color: 0xFF0000, specular : 0x333333, reflectivity : 0.1 } );
	
	
	var sphere12 = new THREE.Mesh(new THREE.SphereGeometry( rayonFer, 32, 32 ), sphereMaterial6 );
	var sphere13 = new THREE.Mesh(new THREE.SphereGeometry( rayonFer, 32, 32 ), sphereMaterial6 );
	var sphere14 = new THREE.Mesh(new THREE.SphereGeometry( rayonFer, 32, 32 ), sphereMaterial6 );
	var sphere15 = new THREE.Mesh(new THREE.SphereGeometry( rayonFer, 32, 32 ), sphereMaterial6 );
	
	var sphere16 = new THREE.Mesh(new THREE.SphereGeometry( rayonOxygene, 32, 32 ), sphereMaterial7 );
	var sphere17 = new THREE.Mesh(new THREE.SphereGeometry( rayonOxygene, 32, 32 ), sphereMaterial7 );
	var sphere18 = new THREE.Mesh(new THREE.SphereGeometry( rayonOxygene, 32, 32 ), sphereMaterial7 );
	var sphere19 = new THREE.Mesh(new THREE.SphereGeometry( rayonOxygene, 32, 32 ), sphereMaterial7 );
	var sphere20 = new THREE.Mesh(new THREE.SphereGeometry( rayonOxygene, 32, 32 ), sphereMaterial7 );
	var sphere21 = new THREE.Mesh(new THREE.SphereGeometry( rayonOxygene, 32, 32 ), sphereMaterial7 );
	
	//FeO3 1
	
	sphere12.position.x = 0;
	sphere12.position.y = 1;
	sphere12.position.z = 0;
	markerRoot5.add(sphere12);
	
	sphere16.position.x = sphere12.position.x - coupedecaleFerOxy;
	sphere16.position.y = sphere12.position.y + coupedecaleFerOxy;
	sphere16.position.z = sphere12.position.z;
	markerRoot5.add(sphere16);
	
	sphere17.position.x = sphere12.position.x + coupedecaleFerOxy;
	sphere17.position.y = sphere12.position.y + coupedecaleFerOxy;
	sphere17.position.z = sphere12.position.z;
	markerRoot5.add(sphere17);
	
	sphere13.position.x = sphere17.position.x + coupedecaleFerOxy;
	sphere13.position.y = sphere17.position.y - coupedecaleFerOxy;
	sphere13.position.z = sphere17.position.z;
	markerRoot5.add(sphere13);
	
	sphere18.position.x = sphere13.position.x + coupedecaleFerOxy;
	sphere18.position.y = sphere13.position.y + coupedecaleFerOxy;
	sphere18.position.z = sphere13.position.z;
	markerRoot5.add(sphere18);
	
	
	
	//Fe03 2
	sphere14.position.x = -0.5;
	sphere14.position.y = 0.5;
	sphere14.position.z = -0.5;
	markerRoot5.add(sphere14);
	
	sphere19.position.x = sphere14.position.x - coupedecaleFerOxy;
	sphere19.position.y = sphere14.position.y + coupedecaleFerOxy;
	sphere19.position.z = sphere14.position.z;
	markerRoot5.add(sphere19);
	
	sphere20.position.x = sphere14.position.x + coupedecaleFerOxy;
	sphere20.position.y = sphere14.position.y + coupedecaleFerOxy;
	sphere20.position.z = sphere14.position.z;
	markerRoot5.add(sphere20);
	
	sphere15.position.x = sphere20.position.x + coupedecaleFerOxy;
	sphere15.position.y = sphere20.position.y - coupedecaleFerOxy;
	sphere15.position.z = sphere20.position.z;
	markerRoot5.add(sphere15);
	
	sphere21.position.x = sphere15.position.x + coupedecaleFerOxy;
	sphere21.position.y = sphere15.position.y + coupedecaleFerOxy;
	sphere21.position.z = sphere15.position.z;
	markerRoot5.add(sphere21);
	
	sphere12.castShadow = true;
	sphere13.castShadow = true;
	sphere14.castShadow = true;
	sphere15.castShadow = true;
	sphere16.castShadow = true;
	sphere17.castShadow = true;
	sphere18.castShadow = true;
	sphere19.castShadow = true;
	sphere20.castShadow = true;
	sphere21.castShadow = true;

	
	sphere12.receiveShadow = true;
	sphere13.receiveShadow = true;
	sphere14.receiveShadow = true;
	sphere15.receiveShadow = true;
	sphere16.receiveShadow = true;
	sphere17.receiveShadow = true;
	sphere18.receiveShadow = true;
	sphere19.receiveShadow = true;
	sphere20.receiveShadow = true;
	sphere21.receiveShadow = true;

	spheretab6[0] = sphere12;
	spheretab6[1] = sphere13;
	spheretab6[2] = sphere14;
	spheretab6[3] = sphere15;
	spheretab6[4] = sphere16;
	spheretab6[5] = sphere17;
	spheretab6[6] = sphere18;
	spheretab6[7] = sphere19;
	spheretab6[8] = sphere20;
	spheretab6[9] = sphere21;
}




function update(){
	// update artoolkit on every frame
	if ( arToolkitSource.ready !== false )
		arToolkitContext.update( arToolkitSource.domElement );
	
	
	if (markerRoot1.visible && markerRoot2.visible){
		// console.log("vecteur marker 1X = " + markerRoot1.getWorldPosition().x);
		// console.log("vecteur marker 1Y = " + markerRoot1.getWorldPosition().y);
		// console.log("vecteur marker 1 Z= " + markerRoot1.getWorldPosition().z);
		// console.log("vecteur marker 2 = " + markerRoot2.getWorldPosition());
		
		var carreXvecteur = Math.pow(markerRoot1.getWorldPosition().x - markerRoot2.getWorldPosition().x,2);
		var carreYvecteur = Math.pow(markerRoot1.getWorldPosition().y - markerRoot2.getWorldPosition().y,2);
		var carreZvecteur = Math.pow(markerRoot1.getWorldPosition().z - markerRoot2.getWorldPosition().z,2);
		
		var racineVecteur = Math.sqrt((Math.pow(markerRoot1.getWorldPosition().x - markerRoot2.getWorldPosition().x,2)) + (Math.pow(markerRoot1.getWorldPosition().y - markerRoot2.getWorldPosition().y,2)) + (Math.pow(markerRoot1.getWorldPosition().z - markerRoot2.getWorldPosition().z,2)));
		
		// console.log("carreXvecteur = " + carreXvecteur);
		// console.log("carreYvecteur= " + carreYvecteur);
		// console.log("carreZvecteur = " + carreZvecteur);
		//console.log("Racine = " + racineVecteur);
		
		if(racineVecteur < 1.95) {
			
			if (ok1 == false) {
				markerRoot1.remove(spheretab[0]);
				markerRoot1.remove(spheretab[1]);
				markerRoot2.remove(spheretab[2]);
				
				dioxydedecarbone();
				
				ok1 = true;
			}
			
			
			
		}
		else{
			if(ok1 == true) {
				markerRoot1.add(spheretab[0]);
				markerRoot1.add(spheretab[1]);
				markerRoot2.add(spheretab[2]);
				
				markerRoot1.remove(spheretab2[0]);
				markerRoot1.remove(spheretab2[1]);
				markerRoot1.remove(spheretab2[2]);
				
				//remove dioxyde de cardone
				ok1 = false;
			}
			
		}
		
		
	}
	
	
}

function update2() {
	// update artoolkit on every frame
	if ( arToolkitSource.ready !== false )
		arToolkitContext.update( arToolkitSource.domElement );
	
	if (markerRoot3.visible && markerRoot4.visible){
		// console.log("vecteur marker 1X = " + markerRoot1.getWorldPosition().x);
		// console.log("vecteur marker 1Y = " + markerRoot1.getWorldPosition().y);
		// console.log("vecteur marker 1 Z= " + markerRoot1.getWorldPosition().z);
		// console.log("vecteur marker 2 = " + markerRoot2.getWorldPosition());
		
		var carreXvecteur = Math.pow(markerRoot3.getWorldPosition().x - markerRoot4.getWorldPosition().x,2);
		var carreYvecteur = Math.pow(markerRoot3.getWorldPosition().y - markerRoot4.getWorldPosition().y,2);
		var carreZvecteur = Math.pow(markerRoot3.getWorldPosition().z - markerRoot4.getWorldPosition().z,2);
		
		var racineVecteur = Math.sqrt((Math.pow(markerRoot3.getWorldPosition().x - markerRoot4.getWorldPosition().x,2)) + (Math.pow(markerRoot3.getWorldPosition().y - markerRoot4.getWorldPosition().y,2)) + (Math.pow(markerRoot3.getWorldPosition().z - markerRoot4.getWorldPosition().z,2)));
		
		// console.log("carreXvecteur = " + carreXvecteur);
		// console.log("carreYvecteur= " + carreYvecteur);
		// console.log("carreZvecteur = " + carreZvecteur);
		// console.log("Racine = " + racineVecteur);
		
		if(racineVecteur < 1.95) {
			
			if (ok2 == false) {
				markerRoot3.remove(spheretab3[0]);
				markerRoot3.remove(spheretab3[1]);
				
				markerRoot4.remove(spheretab3[2]);
				markerRoot4.remove(spheretab3[3]);
				markerRoot4.remove(spheretab3[4]);
				markerRoot4.remove(spheretab3[5]);
				markerRoot4.remove(spheretab3[6]);
				markerRoot4.remove(spheretab3[7]);
				
				ammoniac();
				
				ok2 = true;
			}
			
			
			
		}
		else{
			if(ok2 == true) {
				markerRoot3.add(spheretab3[0]);
				markerRoot3.add(spheretab3[1]);
				
				markerRoot4.add(spheretab3[2]);
				markerRoot4.add(spheretab3[3]);
				markerRoot4.add(spheretab3[4]);
				markerRoot4.add(spheretab3[5]);
				markerRoot4.add(spheretab3[6]);
				markerRoot4.add(spheretab3[7]);
				
				markerRoot3.remove(spheretab4[0]);
				markerRoot3.remove(spheretab4[1]);
				markerRoot3.remove(spheretab4[2]);
				markerRoot3.remove(spheretab4[3]);
				markerRoot3.remove(spheretab4[4]);
				markerRoot3.remove(spheretab4[5]);
				markerRoot3.remove(spheretab4[6]);
				markerRoot3.remove(spheretab4[7]);
				
				
				//remove ammoniac
				ok2 = false;
			}
			
		}
		
		
	}
}

function update3 () {
	// update artoolkit on every frame
	if ( arToolkitSource.ready !== false )
		arToolkitContext.update( arToolkitSource.domElement );
	
	if (markerRoot5.visible && markerRoot6.visible){
		// console.log("vecteur marker 1X = " + markerRoot1.getWorldPosition().x);
		// console.log("vecteur marker 1Y = " + markerRoot1.getWorldPosition().y);
		// console.log("vecteur marker 1 Z= " + markerRoot1.getWorldPosition().z);
		// console.log("vecteur marker 2 = " + markerRoot2.getWorldPosition());
		
		var carreXvecteur = Math.pow(markerRoot5.getWorldPosition().x - markerRoot6.getWorldPosition().x,2);
		var carreYvecteur = Math.pow(markerRoot5.getWorldPosition().y - markerRoot6.getWorldPosition().y,2);
		var carreZvecteur = Math.pow(markerRoot5.getWorldPosition().z - markerRoot6.getWorldPosition().z,2);
		
		var racineVecteur = Math.sqrt((Math.pow(markerRoot5.getWorldPosition().x - markerRoot6.getWorldPosition().x,2)) + (Math.pow(markerRoot5.getWorldPosition().y - markerRoot6.getWorldPosition().y,2)) + (Math.pow(markerRoot5.getWorldPosition().z - markerRoot6.getWorldPosition().z,2)));
		
		// console.log("carreXvecteur = " + carreXvecteur);
		// console.log("carreYvecteur= " + carreYvecteur);
		// console.log("carreZvecteur = " + carreZvecteur);
		// console.log("Racine = " + racineVecteur);
		
		if(racineVecteur < 1.95) {
			
			if (ok3 == false) {
				markerRoot5.remove(spheretab5[0]);
				markerRoot5.remove(spheretab5[1]);
				markerRoot5.remove(spheretab5[2]);
				markerRoot5.remove(spheretab5[3]);
				
				markerRoot6.remove(spheretab5[4]);
				markerRoot6.remove(spheretab5[5]);
				markerRoot6.remove(spheretab5[6]);
				markerRoot6.remove(spheretab5[7]);
				markerRoot6.remove(spheretab5[8]);
				markerRoot6.remove(spheretab5[9]);
				
				oxydeferrique();
				
				ok3 = true;
			}
			
			
			
		}
		else{
			if(ok3 == true) {
				markerRoot5.add(spheretab5[0]);
				markerRoot5.add(spheretab5[1]);
				markerRoot5.add(spheretab5[2]);
				markerRoot5.add(spheretab5[3]);
				
				markerRoot6.add(spheretab5[4]);
				markerRoot6.add(spheretab5[5]);
				markerRoot6.add(spheretab5[6]);
				markerRoot6.add(spheretab5[7]);
				markerRoot6.add(spheretab5[8]);
				markerRoot6.add(spheretab5[9]);
				
				markerRoot5.remove(spheretab6[0]);
				markerRoot5.remove(spheretab6[1]);
				markerRoot5.remove(spheretab6[2]);
				markerRoot5.remove(spheretab6[3]);
				markerRoot5.remove(spheretab6[4]);
				markerRoot5.remove(spheretab6[5]);
				markerRoot5.remove(spheretab6[6]);
				markerRoot5.remove(spheretab6[7]);
				markerRoot5.remove(spheretab6[8]);
				markerRoot5.remove(spheretab6[9]);
				
				
				//remove oxydeferrique
				ok3 = false;
			}
			
		}
		
		
	}
}

function update4() {
	// update artoolkit on every frame
	if ( arToolkitSource.ready !== false )
		arToolkitContext.update( arToolkitSource.domElement );
	
	if (markerRoot7.visible && markerRoot8.visible){
		// console.log("vecteur marker 1X = " + markerRoot1.getWorldPosition().x);
		// console.log("vecteur marker 1Y = " + markerRoot1.getWorldPosition().y);
		// console.log("vecteur marker 1 Z= " + markerRoot1.getWorldPosition().z);
		// console.log("vecteur marker 2 = " + markerRoot2.getWorldPosition());
		
		// var carreXvecteur = Math.pow(markerRoot7.getWorldPosition().x - markerRoot8.getWorldPosition().x,2);
		// var carreYvecteur = Math.pow(markerRoot7.getWorldPosition().y - markerRoot8.getWorldPosition().y,2);
		// var carreZvecteur = Math.pow(markerRoot7.getWorldPosition().z - markerRoot8.getWorldPosition().z,2);
		
		var racineVecteur = Math.sqrt((Math.pow(markerRoot7.getWorldPosition().x - markerRoot8.getWorldPosition().x,2)) + (Math.pow(markerRoot7.getWorldPosition().y - markerRoot8.getWorldPosition().y,2)) + (Math.pow(markerRoot7.getWorldPosition().z - markerRoot8.getWorldPosition().z,2)));
		
		// console.log("carreXvecteur = " + carreXvecteur);
		// console.log("carreYvecteur= " + carreYvecteur);
		// console.log("carreZvecteur = " + carreZvecteur);
		// console.log("Racine = " + racineVecteur);
		
		if(racineVecteur < 1.95) {
			
			if (ok4 == false) {
				markerRoot7.remove(spheretab7[0]);
				markerRoot7.remove(spheretab7[1]);
				markerRoot7.remove(spheretab7[2]);
				markerRoot7.remove(spheretab7[3]);
				markerRoot7.remove(spheretab7[4]);
				markerRoot7.remove(spheretab7[5]);
				markerRoot7.remove(spheretab7[6]);
				markerRoot7.remove(spheretab7[7]);
				markerRoot7.remove(spheretab7[8]);
				markerRoot7.remove(spheretab7[9]);
				markerRoot7.remove(spheretab7[10]);
				markerRoot7.remove(spheretab7[11]);
				markerRoot7.remove(spheretab7[12]);
				markerRoot7.remove(spheretab7[13]);
				markerRoot7.remove(spheretab7[14]);
				markerRoot7.remove(spheretab7[15]);
				markerRoot7.remove(spheretab7[16]);
				markerRoot7.remove(spheretab7[17]);
				markerRoot7.remove(spheretab7[18]);
				markerRoot7.remove(spheretab7[19]);
				markerRoot7.remove(spheretab7[46]);
				markerRoot7.remove(spheretab7[47]);
				markerRoot7.remove(spheretab7[48]);
				markerRoot7.remove(spheretab7[49]);
				markerRoot7.remove(spheretab7[50]);
				markerRoot7.remove(spheretab7[51]);
				markerRoot7.remove(spheretab7[52]);
				markerRoot7.remove(spheretab7[53]);
				
				
				markerRoot8.remove(spheretab7[20]);
				markerRoot8.remove(spheretab7[21]);
				markerRoot8.remove(spheretab7[22]);
				markerRoot8.remove(spheretab7[23]);
				markerRoot8.remove(spheretab7[24]);
				markerRoot8.remove(spheretab7[25]);
				markerRoot8.remove(spheretab7[26]);
				markerRoot8.remove(spheretab7[27]);
				markerRoot8.remove(spheretab7[28]);
				markerRoot8.remove(spheretab7[29]);
				markerRoot8.remove(spheretab7[30]);
				markerRoot8.remove(spheretab7[31]);
				markerRoot8.remove(spheretab7[32]);
				markerRoot8.remove(spheretab7[33]);
				markerRoot8.remove(spheretab7[34]);
				markerRoot8.remove(spheretab7[35]);
				markerRoot8.remove(spheretab7[36]);
				markerRoot8.remove(spheretab7[37]);
				markerRoot8.remove(spheretab7[38]);
				markerRoot8.remove(spheretab7[39]);
				markerRoot8.remove(spheretab7[40]);
				markerRoot8.remove(spheretab7[41]);
				markerRoot8.remove(spheretab7[42]);
				markerRoot8.remove(spheretab7[43]);
				markerRoot8.remove(spheretab7[44]);
				markerRoot8.remove(spheretab7[45]);
				
				dioeau();
				
				ok4 = true;
			}
			
			
			
		}
		else{
			if(ok4 == true) {
				markerRoot7.remove(spheretab8[0]);
				markerRoot7.remove(spheretab8[1]);
				markerRoot7.remove(spheretab8[2]);
				markerRoot7.remove(spheretab8[3]);
				markerRoot7.remove(spheretab8[4]);
				markerRoot7.remove(spheretab8[5]);
				markerRoot7.remove(spheretab8[6]);
				markerRoot7.remove(spheretab8[7]);
				markerRoot7.remove(spheretab8[8]);
				markerRoot7.remove(spheretab8[9]);
				markerRoot7.remove(spheretab8[10]);
				markerRoot7.remove(spheretab8[11]);
				markerRoot7.remove(spheretab8[12]);
				markerRoot7.remove(spheretab8[13]);
				markerRoot7.remove(spheretab8[14]);
				markerRoot7.remove(spheretab8[15]);
				markerRoot7.remove(spheretab8[16]);
				markerRoot7.remove(spheretab8[17]);
				markerRoot7.remove(spheretab8[18]);
				markerRoot7.remove(spheretab8[19]);
				markerRoot7.remove(spheretab8[46]);
				markerRoot7.remove(spheretab8[47]);
				markerRoot7.remove(spheretab8[48]);
				markerRoot7.remove(spheretab8[49]);
				markerRoot7.remove(spheretab8[50]);
				markerRoot7.remove(spheretab8[51]);
				markerRoot7.remove(spheretab8[52]);
				markerRoot7.remove(spheretab8[53]);
				
				
				markerRoot7.remove(spheretab8[20]);
				markerRoot7.remove(spheretab8[21]);
				markerRoot7.remove(spheretab8[22]);
				markerRoot7.remove(spheretab8[23]);
				markerRoot7.remove(spheretab8[24]);
				markerRoot7.remove(spheretab8[25]);
				markerRoot7.remove(spheretab8[26]);
				markerRoot7.remove(spheretab8[27]);
				markerRoot7.remove(spheretab8[28]);
				markerRoot7.remove(spheretab8[29]);
				markerRoot7.remove(spheretab8[30]);
				markerRoot7.remove(spheretab8[31]);
				markerRoot7.remove(spheretab8[32]);
				markerRoot7.remove(spheretab8[33]);
				markerRoot7.remove(spheretab8[34]);
				markerRoot7.remove(spheretab8[35]);
				markerRoot7.remove(spheretab8[36]);
				markerRoot7.remove(spheretab8[37]);
				markerRoot7.remove(spheretab8[38]);
				markerRoot7.remove(spheretab8[39]);
				markerRoot7.remove(spheretab8[40]);
				markerRoot7.remove(spheretab8[41]);
				markerRoot7.remove(spheretab8[42]);
				markerRoot7.remove(spheretab8[43]);
				markerRoot7.remove(spheretab8[44]);
				markerRoot7.remove(spheretab8[45]);
				
				
				markerRoot7.add(spheretab7[0]);
				markerRoot7.add(spheretab7[1]);
				markerRoot7.add(spheretab7[2]);
				markerRoot7.add(spheretab7[3]);
				markerRoot7.add(spheretab7[4]);
				markerRoot7.add(spheretab7[5]);
				markerRoot7.add(spheretab7[6]);
				markerRoot7.add(spheretab7[7]);
				markerRoot7.add(spheretab7[8]);
				markerRoot7.add(spheretab7[9]);
				markerRoot7.add(spheretab7[10]);
				markerRoot7.add(spheretab7[11]);
				markerRoot7.add(spheretab7[12]);
				markerRoot7.add(spheretab7[13]);
				markerRoot7.add(spheretab7[14]);
				markerRoot7.add(spheretab7[15]);
				markerRoot7.add(spheretab7[16]);
				markerRoot7.add(spheretab7[17]);
				markerRoot7.add(spheretab7[18]);
				markerRoot7.add(spheretab7[19]);
				markerRoot7.add(spheretab7[46]);
				markerRoot7.add(spheretab7[47]);
				markerRoot7.add(spheretab7[48]);
				markerRoot7.add(spheretab7[49]);
				markerRoot7.add(spheretab7[50]);
				markerRoot7.add(spheretab7[51]);
				markerRoot7.add(spheretab7[52]);
				markerRoot7.add(spheretab7[53]);
				
				
				markerRoot8.add(spheretab7[20]);
				markerRoot8.add(spheretab7[21]);
				markerRoot8.add(spheretab7[22]);
				markerRoot8.add(spheretab7[23]);
				markerRoot8.add(spheretab7[24]);
				markerRoot8.add(spheretab7[25]);
				markerRoot8.add(spheretab7[26]);
				markerRoot8.add(spheretab7[27]);
				markerRoot8.add(spheretab7[28]);
				markerRoot8.add(spheretab7[29]);
				markerRoot8.add(spheretab7[30]);
				markerRoot8.add(spheretab7[31]);
				markerRoot8.add(spheretab7[32]);
				markerRoot8.add(spheretab7[33]);
				markerRoot8.add(spheretab7[34]);
				markerRoot8.add(spheretab7[35]);
				markerRoot8.add(spheretab7[36]);
				markerRoot8.add(spheretab7[37]);
				markerRoot8.add(spheretab7[38]);
				markerRoot8.add(spheretab7[39]);
				markerRoot8.add(spheretab7[40]);
				markerRoot8.add(spheretab7[41]);
				markerRoot8.add(spheretab7[42]);
				markerRoot8.add(spheretab7[43]);
				markerRoot8.add(spheretab7[44]);
				markerRoot8.add(spheretab7[45]);
				
				
				//remove dioeau
				ok4 = false;
			}
			
		}
		
		
	}
}

function update5() {
	// update artoolkit on every frame
	if ( arToolkitSource.ready !== false )
		arToolkitContext.update( arToolkitSource.domElement );
	
	
	if (markerRoot9.visible && markerRoot10.visible){
		// console.log("vecteur marker 1X = " + markerRoot1.getWorldPosition().x);
		// console.log("vecteur marker 1Y = " + markerRoot1.getWorldPosition().y);
		// console.log("vecteur marker 1 Z= " + markerRoot1.getWorldPosition().z);
		// console.log("vecteur marker 2 = " + markerRoot2.getWorldPosition());
		
		var carreXvecteur = Math.pow(markerRoot9.getWorldPosition().x - markerRoot10.getWorldPosition().x,2);
		var carreYvecteur = Math.pow(markerRoot9.getWorldPosition().y - markerRoot10.getWorldPosition().y,2);
		var carreZvecteur = Math.pow(markerRoot9.getWorldPosition().z - markerRoot10.getWorldPosition().z,2);
		
		var racineVecteur = Math.sqrt((Math.pow(markerRoot9.getWorldPosition().x - markerRoot10.getWorldPosition().x,2)) + (Math.pow(markerRoot9.getWorldPosition().y - markerRoot10.getWorldPosition().y,2)) + (Math.pow(markerRoot9.getWorldPosition().z - markerRoot10.getWorldPosition().z,2)));
		
		// console.log("carreXvecteur = " + carreXvecteur);
		// console.log("carreYvecteur= " + carreYvecteur);
		// console.log("carreZvecteur = " + carreZvecteur);
		//console.log("Racine = " + racineVecteur);
		
		if(racineVecteur < 1.95) {
			
			if (ok1 == false) {
				markerRoot9.remove(spheretab9[0]);
				markerRoot9.remove(spheretab9[1]);
				markerRoot9.remove(spheretab9[2]);
				markerRoot9.remove(spheretab9[3]);
				markerRoot9.remove(spheretab9[4]);
				markerRoot10.remove(spheretab9[5]);
				markerRoot10.remove(spheretab9[6]);
				markerRoot10.remove(spheretab9[7]);
				markerRoot10.remove(spheretab9[8]);
				
				console.log("< 1.95");
				eaudio();
				
				ok1 = true;
			}
			
			
			
		}
		else{
			if(ok1 == true) {
				markerRoot9.add(spheretab9[0]);
				markerRoot9.add(spheretab9[1]);
				markerRoot9.add(spheretab9[2]);
				markerRoot9.add(spheretab9[3]);
				markerRoot9.add(spheretab9[4]);
				markerRoot10.add(spheretab9[5]);
				markerRoot10.add(spheretab9[6]);
				markerRoot10.add(spheretab9[7]);
				markerRoot10.add(spheretab9[8]);
				
				
				markerRoot9.remove(spheretab10[0]);
				markerRoot9.remove(spheretab10[1]);
				markerRoot9.remove(spheretab10[2]);
				markerRoot9.remove(spheretab10[3]);
				markerRoot9.remove(spheretab10[4]);
				markerRoot9.remove(spheretab10[5]);
				markerRoot9.remove(spheretab10[6]);
				markerRoot9.remove(spheretab10[7]);
				markerRoot9.remove(spheretab10[8]);
				
				console.log("suppr eadio met original");
				
				//remove eaudio
				ok1 = false;
			}
			
		}
		
		
	}
}
function render()
{
	renderer.render( scene, camera );
	// spheretab[0].position.x -= 0.0001;
	// mouvement();
	
	// console.log("distance = " + Math.sqrt((Math.pow((spheretab[2].position.x - spheretab[0].position.x),2)) + (Math.pow((spheretab[2].position.y - spheretab[0].position.y),2)) + (Math.pow((spheretab[2].position.z - spheretab[0].position.z),2))));
}


function animate()
{
	requestAnimationFrame(animate);
	deltaTime = clock.getDelta();
	totalTime += deltaTime;
	update();
	render();
	update2();
	update3();
	update4();
	update5();
}
initialize();
animate();