# Observation d’une vingtaine de molécules 

![](mollecules2.jpg)

## Un projet de : 
- Maxime Goumeziane (2A info IUT St-Dié)

## Encadré par : 
- Pierre-Frédéric Villard 

## Commandité par : 
- Mme Laurence Colin

## Installer l'application
- Installez **Mozilla Firefox** (Navigateur Internet)
- Dans la barre de recherche de Firefox, en haut :
  - Tapez : ```about:config```
  - Appuyez sur la touche **Entrée**
  - Cliquez sur **Accepter le risque et poursuivre** si ce message apparait
  - Ecrivez dans la barre de recherche ```security.fileuri```
  - Double-cliquez sur l’onglet qui apparait (```security.fileuri.strict_origin_policy```). La valeur ```true``` devient ```false```
  - Maintenant, remplacez ce que vous avez écrit dans la barre de recherche par ```webgl.force- enabled```
  - Double-cliquez sur l’onglet qui apparait. La valeur ```true``` devient ```false```
  - Ouvrez le fichier ```Projet_Atome.html``` avec **Mozilla Firefox**.

## Exécuter l'application 

- L’application vous demande d’utiliser votre caméra :
  - Sur l’ordinateur, choisissez la caméra que vous voulez utiliser (connectée à l’ordinateur).
  - Sur le téléphone, choisissez la caméra avant ou arrière.

- 2 solutions :
  - Ouvrez le fichier ```Projet_Atome.html``` avec **Mozilla Firefox**
  - Allez sur la page [https://seshat.gitlabpages.inria.fr/outils_pedagogiques_ra_rv ](https://seshat.gitlabpages.inria.fr/outils_pedagogiques_ra_rv )

**Imprimer les marqueurs se trouvant [ici](marqueurs)**