# Fleur (avec sépales, pétale, étamines et pistil + pollinisation)

![](fleur.jpg)

## Un projet de : 
- Lucie Septier (LP IG3D)

## Encadré par : 
- Pierre-Frédéric Villard 
- Aline Caspary

## Commandité par : 
- Mikael CHARLET

## Installer l'application sur l'Oculus Go

```
adb install fleurRA.apk
```

**Imprimer les marqueurs se trouvant [ici](marqueurs)**