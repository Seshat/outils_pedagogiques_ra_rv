# Applications de réalité augmentée

## Matériel
- Tablette Android
- SmartPhone Android

## Preparation de la tablette
1. Aller dans les **configurations**
2. Aller dans **A propos de la tablette**
3. Taper sur **Numéro de build** jusqu'à débloquage
4. Aller dans **Options pour les développeurs**
5. Autorisez **Deboggage USB**
6. Redémarrer la tablette
7. ‘Revoke USB debugging authorizations’. Tap it and for good measure, restart your phone.


## Installation d'une application

1. Connecter le casque
2. Vérifier que le casque est bien visible sur le port USB
```
adb devices
```
3. Utiliser la commande :
```
adb kill-server
```
4. Connecter le casque
5. Utiliser la commande :
```
adb start-server
```
6. Connecter le casque
7. Autoriser la connection
8. Installer le fichier *nom_du_fichier.apk*
```
adb install nom_du_fichier.apk
```

## Applications
- [Fleur1](Fleur1) : Fleur (avec sépales, pétale, étamines et pistil + pollinisation)
- [Fleur2](Fleur2) : Fleur (avec sépales, pétale, étamines et pistil + pollinisation)
- [Chimie](Chimie) : Réactions chimiques entre molécules par plusieurs marqueurs en RA (but : écrire l’équation de réaction à partir de marqueurs qui se croisent)
- [Molecules1](Molecules1) : Observation d’une vingtaine de molécules en 3D par RA marqueur (but : écrire la formule brute de la molécule en l'observant en 3D) 
- [Molecules2](Molecules2) : Observation d’une vingtaine de molécules en 3D par RA marqueur (but : écrire la formule brute de la molécule en l'observant en 3D) 