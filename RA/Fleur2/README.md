# Fleur (avec sépales, pétale, étamines et pistil + pollinisation)

![](fleur.jpg)

## Un projet de : 
- Florian Gagnaire(LP IG3D)

## Encadré par : 
- Pierre-Frédéric Villard 
- Laurence Moreau

## Commandité par : 
- Mikael CHARLET

## Installer l'application sur l'appareil

```
adb install Fleur.apk
```

**Imprimer les marqueurs se trouvant [ici](marqueurs)**