# Observation d’une vingtaine de molécules 

![](Molecules1.jpg)

## Un projet de : 
- Alexandre Abbes (LP IG3D)

## Encadré par : 
- Pierre-Frédéric Villard 
- Laurence Moreau

## Commandité par : 
- Mme Laurence Colin

## Installer l'application sur l'Oculus Go

```
adb install RAMolecule.apk
```

**Imprimer les marqueurs se trouvant [ici](marqueurs)**