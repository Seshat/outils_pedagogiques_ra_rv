# Réactions chimiques entre molécules

![](chimie.jpg)

## Un projet de : 
- Amélie Soulagnat (LP IG3D)

## Encadré par : 
- Pierre-Frédéric Villard 
- Laurence Moreau

## Commandité par : 
- Mme Laurence Colin

## Installer l'application sur l'appareil

```
adb install Moleculium.apk
```

**Imprimer les marqueurs se trouvant [ici](marqueurs)**